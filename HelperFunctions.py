class Helper:

    # Adds zeros to a given range
    @staticmethod
    def populate_range_char(range: tuple, zeros_num: int, special_char: str):
        if len(special_char) > 1:
            raise Exception('Char len must be 1')
        if len(range) > 2:
            raise Exception('Range must have only 2 elements.')
        range_str = f'{sorted(range)[0]}-{sorted(range)[1]}'
        if len(range_str) == zeros_num:
            return range_str
        elif len(range_str) > zeros_num:
            # decrease 1 for '-'
            raise Exception(
                f'Range must include numbers with total len of {zeros_num - 1}')

        return special_char*(zeros_num - len(range_str)) + range_str
    # In POC as well, I would suggest to move it to HelperFunctions or something and import it here

    # Function decodes something like 000000015-65
    # Returns (15, 65)
    @staticmethod
    def decode_range_str(s: str, special_char: str):
        if len(special_char) > 1:
            raise Exception('Char len must be 1.')

        s = s.replace(special_char, '')
        s = s.split('-')
        if len(s) != 2:
            raise Exception('Range must have only 2 elements!')

        return (int(s[0]), int(s[1]))

    @staticmethod
    def add_zeros(num: int, zeros: int):
        if len(str(num)) > zeros:
            raise Exception(f'Number {num} len must be shorter than {zeros}')
        if zeros < 0 or num < 0:
            raise Exception(f'Zeros number or number must be above 0')
        return '0'*(zeros - len(str(num))) + str(num)
