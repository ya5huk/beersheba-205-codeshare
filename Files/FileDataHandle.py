import base64
import os
import pickle
from pathlib import Path
# from .FileSync import RepoListener

# FileDataHandle is the only class who has functions interacting with file data

IGNORE_FILE_EXTENSIONS = '.idea, .git'


class FileDataHandle:
    # Should be a static class with read_all_file, write_to_files functions
    # I'll add for now, but functions are still left in POC to not disturb Ron's code

    @staticmethod
    def write_to_files(path, files):
        Path(path).mkdir(parents=True, exist_ok=True)
        for filename, file_content in files.items():
            # Sync directory recursively
            if file_content[:3] == b'DIR':
                dir_files = pickle.loads(file_content[3:])
                FileDataHandle.write_to_files(path + filename + '/', dir_files)

            elif file_content[:3] == b'FIL':
                f = open(path + filename, "wb")
                f.write(file_content[3:])
                f.close()

    @staticmethod
    def read_all_files(path):
        file_dict = {}
        files = os.listdir(path)
        for file in files:
            if os.path.isdir(path + file):
                file_dict[file] = b'DIR' + \
                    pickle.dumps(
                        FileDataHandle.read_all_files(path + file + '/'))
            else:
                extension = file.split('.')[-1]
                # Prevent common cache files
                if (file[:2] != '~$' and file[0] != '~' and file[-1] != '~') and extension not in IGNORE_FILE_EXTENSIONS:
                    f = open(path + file, 'rb')
                    file_content = f.read()
                    f.close()
                    file_dict[file] = b'FIL' + file_content
        return file_dict

    # Function forces changes that were done on srcpath and saves the new file in savepath
    @staticmethod
    def force_changes(srcpath: str, savepath: str, changes: dict):
        # changes should be {-1: deletions, index: change, ...check func RepoListener.filecontentdiff}
        print('got changes -> ', changes)
        with open(srcpath, 'rb') as f:
            src_content = bytearray(f.read())
        for index, (old, new) in changes.items():
            index = int(index)
            if index == -1:
                # delete everything after old (last index of new file)
                src_content = src_content[:old]
            elif old == '\x00':
                src_content.append(new)
            else:
                src_content[index] = new

        with open(savepath, 'wb+') as f:
            print(f'@ {srcpath}')
            f.write(src_content)

    @staticmethod
    def force_directory_update(*args):
        # repo_listener: RepoListener is imported to lock changes when they occur not by user
        if len(args) != 5:
            raise Exception(
                f'Funtion <force_directory_update> should receive exactly 5 params')
        repo_listener, root_repo, additions, changes, deletions = args
        # Adding
        for addon in additions:
            noroot_filename = list(addon.items())[0][0]
            filename = root_repo + noroot_filename
            filecontent = base64.b64decode(list(addon.items())[0][1])
            path_sections = len(noroot_filename.split('\\'))
            if path_sections > 1:
                # Example: hey/what/s.txt
                #  Dir handle\
                newdirs = ''
                for index, newdir in enumerate(noroot_filename.split('\\')):
                    newdirs += newdir + '\\'
                    fullpath = root_repo + newdirs
                    if not os.path.exists(fullpath[:-1]):
                        print('Creating ', newdirs)
                        if index != path_sections - 1:
                            os.mkdir(fullpath[:-1])
                        else:
                            with open(fullpath[:-1], 'wb') as f:
                                # delete \\ at the end
                                f.write(filecontent[:-1])

                        # Give permissions to new file
                        os.chmod(fullpath[:-1], 0o777)

            try:
                with open(filename, 'wb+') as f:
                    print(f'+ {filename}')
                    f.write(filecontent)
                    os.chmod(filename, 0o777)  # Give permissions to new file
            except Exception as e:
                # Happens when only a directory is created
                # Because we can't really differ between file / dir, An exception will be printed
                print(
                    f'File {fullpath} couldnt be written (Could be a directory)')

        # Deletions
        # Because first files should be deleted and then it's dirs
        for filename in sorted(deletions, reverse=True):
            if filename == '':  # If nothing was deleted
                continue

            try:

                fullpath = root_repo + filename
                if os.path.exists(fullpath):
                    os.remove(fullpath) if not os.path.isdir(
                        fullpath) else os.rmdir(fullpath)
                    print(f'- {fullpath}')
                else:
                    print(f'Path {filename} was not found...')
            except Exception as e:
                print(f'Error removing {filename}... Error ↓\n{e}')

        # Changes
        # Because lack of time I'll recieve changes filenames as new files and just will overwrite
        # So I'll ignore the changes system because it is semi - works

        for change in changes:
            filename = root_repo + list(change.items())[0][0]
            filechanges = list(change.items())[0][1]
            repo_listener.add_outside_changed_file(
                filename)  # So watcher won't send it back
            try:
                FileDataHandle.force_changes(
                    filename, filename, filechanges)
            except Exception as e:
                print(f'Could not force changes on {filename} -> {e}')
