import base64
from inspect import _void
import os
import time
import socket
from .FileDataHandle import FileDataHandle
from . import Constants
import json
import sys
from HelperFunctions import Helper
import threading


class RepoListener:

    def __init__(self, repo: str, watch_delay=Constants.DEFAULT_REPO_WATCH_DELAY, exclude_extensions=[], send_to_sockets=[]):
        # Files that won't be updated for each change
        self.exclude_extensions = exclude_extensions  # TODO Currently not working
        self.repo = repo
        self.watch_delay = watch_delay
        self.cached_files = {}  # {filepath: last_time_it_was_modified}
        # list of sockets to send user changes to
        self.send_changes_sockets: list[socket.socket] = send_to_sockets
        self.input_files_changes = []

        print(f'Repo watch will occur every <{watch_delay}s>')

    def exclude_file_extension(self, ext: str):
        self.exclude_extensions.append(ext)

    def add_changes_socket(self, s: socket.socket):
        self.send_changes_sockets.append(s)

    def add_outside_changed_file(self, filename: str):
        self.input_files_changes.append(filename)

    @staticmethod
    def get_repo_status(root_repo: str, filepath=''):
        # root_repo -> Personal to every peer
        # filepath -> same paths for all peers
        status = {}
        full_path = root_repo + filepath
        if (not os.path.exists(full_path)):
            raise Exception(f'Path {full_path} does not exist')
        files = list(FileDataHandle.read_all_files(full_path).items())

        for filename, filecontent in files:
            if filecontent[:3] == b'FIL':
                saved_path = filepath + filename
                last_modified_time = os.path.getmtime(full_path + filename)
                status[saved_path] = (
                    last_modified_time, filecontent[3:])
            if filecontent[:3] == b'DIR':
                status[filepath + filename +
                       '\\'] = (os.path.getmtime(full_path + filename + '\\'), b'')
                status = {
                    **status, **RepoListener.get_repo_status(root_repo, f'{filepath}{filename}\\')}  # Combining, From now on I want repo to be saved
                # So status will be:
                # {
                #  'doc.txt: (451, b'5454')
                #  'damn/doc.txt: ...
                # }

        return status

    def listen_to_repo_changes(self):

        self.cached_files = RepoListener.get_repo_status(self.repo)
        # Cycle to see what changed
        while True:
            # print(f'Changes that were made in the last {self.watch_delay}s ↓')
            time.sleep(self.watch_delay)
            # list of dicts -> [{filename: change}, ...]
            changed_files = []

            # list of dicts -> [{filename: filecontent}, {filename: filecontent}, ...]
            added_files = []

            deleted_filenames = []  # filenames only
            filenames_iterated = []

            repo_status_items = RepoListener.get_repo_status(self.repo).items()

            # Iterating through all curr files
            for filename, (timestamp, filecontent) in repo_status_items:

                try:

                    # print(
                    #     f'Cached Timestamp of {filename} = {self.cached_files[filename][0]}')
                    # print(
                    #     f'Real Timestamp of {filename} = {os.path.getmtime(self.repo + filename)}')

                    # File was added
                    if filename not in self.cached_files.keys():
                        filepath = f'{self.repo}{filename}'
                        if not os.path.isdir(filepath):
                            with open(filepath, 'rb') as f:
                                added_files.append({filename: f.read()})
                        else:
                            added_files.append({filename: b''})

                    # File was changed
                    elif self.cached_files[filename][0] != timestamp:
                        if self.repo + filename in self.input_files_changes:  # Pass on a change if was made by other peer
                            self.input_files_changes.remove(
                                self.repo + filename)
                            pass
                        else:
                            cached_filecontent = self.cached_files[filename][1]
                            files_diff = RepoListener.filecontent_diff(
                                cached_filecontent, filecontent)
                            if files_diff != {}:  # Clean weird empty changes
                                changed_files.append({filename: files_diff})

                except Exception as e:
                    print(
                        f'Adding/Changing file at current cycle watch caused error -> ', e)

                filenames_iterated.append(filename)

            # Searching for missing files (deleted / moved)
            try:
                for filename in self.cached_files.keys():
                    if filename not in filenames_iterated:
                        deleted_filenames = list(
                            filter(lambda fnm: fnm not in filenames_iterated, list(self.cached_files.keys())))  # filtering files that were found in last change
                for filename in deleted_filenames:
                    del self.cached_files[filename]
            except Exception as e:
                print(f'Finding deleted file at current cycle watch caused error -> ', e)

            # Cache last changes
            for filename, (timestamp, filecontent) in repo_status_items:
                self.cached_files[f'{filename}'] = (timestamp, filecontent)

            if added_files != [] or changed_files != [] or deleted_filenames != []:
                # Actual update send
                self.echo_changes_peer(
                    (added_files, changed_files, deleted_filenames))

    # NOTE
    # Protocol
    # Explanation   CODE  SIZE      ADDITIONS_JSON_ARR                                     SIZE          CHANGES_JSON_ARR                         SIZE          DELETIONS
    # Size           <3><---7-> <--------------------------------dynamic-----------> <---10--> <-----------------dynamic---------------------><--7--><---------dynamic-------->
    # Example        UPD0001500[{a.txt: "b64content"},{money\\ty.txt: "b64content"}]0000152300{filename: changes_json, filename: changes_json}0012300hey.txt,what/d.txt,help.me
    def echo_changes_peer(self, watch_summary: tuple):
        if len(watch_summary) > 3:
            raise Exception(
                'Tuple must only include (added, changed, deleted)')

        # Encode binary to base64 (json does not work with binary)
        additions_list = []
        for addon in watch_summary[0]:
            filename, filecontent = list(addon.keys())[
                0], list(addon.values())[0]
            additions_list.append(
                {f'{filename}': base64.b64encode(filecontent).decode()})
        changes = json.dumps(watch_summary[1])
        # NOTE Because lack of time I'll just add changed to added so it will overwrite them
        # for changes_per_file in watch_summary[1]:
        #     filename = list(changes_per_file.keys())[0]
        #     filepath = self.repo + filename
        #     with open(filepath, 'rb') as f:
        #         content = f.read()

        #     additions_list.append(
        #         {filename: base64.b64encode(content).decode()})

        additions = json.dumps(additions_list)

        deletions = Constants.DELETED_FILES_SEPERATOR.join(watch_summary[2])
        # additions -> [{filename: content}, {filename: content}, ...]
        # changes -> [{filename: changes_dict}, {filename: changes_dict}, ...]
        # deletions = [filename, filename, filename, ...]

        additions_len = Helper.add_zeros(
            len(additions), Constants.MAX_ADDITIONS_LEN)
        changes_len = Helper.add_zeros(len(changes), Constants.MAX_CHANGES_LEN)
        deletions_len = Helper.add_zeros(
            len(deletions), Constants.MAX_DELETIONS_LEN)

        finalmsg = ''
        code = 'UPD'
        finalmsg += code + additions_len + additions + \
            changes_len + changes + deletions_len + deletions

        for sock in self.send_changes_sockets:
            sock.send(finalmsg.encode())

        # Finding changes -> we could just find changes in the overall repo bytes dump but it would be a lot slower
        # Function finds how fc2 changed compared to fc1
        # Returns dictionary:
        # {-1: (start_deleting_index, end_deleting_index), index: (old_byte, new_bute)}
        # if old_byte is \x00, there is an additon

    @staticmethod
    def filecontent_diff(fc1: bytes, fc2: bytes) -> dict:
        index = 0
        changes = {}
        fc2_len = len(fc2)
        fc1_len = len(fc1)

        if fc1_len > fc2_len:
            # -1 is saved index for showing that indexes were deleted
            changes[-1] = (fc2_len, fc1_len)

        while index < fc2_len:
            fc2_byte = fc2[index]
            if index < fc1_len:
                fc1_byte = fc1[index]
            else:
                changes[index] = ('\x00', fc2_byte)
                index += 1
                continue  # Restart loop

            if fc1_byte != fc2_byte:
                # {change_offset: (old_byte, new_byte)}
                changes[index] = (fc1_byte, fc2_byte)

            index += 1
        return changes

    # Function to nicely print changes that occured in the last watch cycle
    @staticmethod
    def print_repo_change_cycle(added_files: list, changed_files: list, deleted_filenames: list):

        print('Repo watch cycle changes ↓')

        edited_added_files = []
        for file in added_files:
            # Every dict in added_files has only one key-value pair
            filename, filedata = list(file.keys())[0], list(file.values())[0]
            if len(filedata) > Constants.ONE_KILOBYTE:
                edited_added_files.append(
                    {filename: b'BIGGER THAN 1 KB'})
            else:
                edited_added_files.append({filename: filedata})

        print(
            f'ADD -> {edited_added_files}') if added_files != [] else ''
        print(
            f'CHANGE -> {changed_files}') if changed_files != [] else ''
        print(
            f'DEL -> {deleted_filenames}') if deleted_filenames != [] else ''
