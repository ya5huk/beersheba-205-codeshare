
import base64
from inspect import _void
import collections
import os
from HelperFunctions import Helper
from .Constants import *
from .FileDataHandle import FileDataHandle

# NOTE
# InitFilesHandle focuses on transfering init files when logged in from a peer to another
# Transfering data was tested on 14 users (maybe the sun doesn't shine after 15 but ig we never know)
#
# *not ultra optimized but works
# >>>>>>>> BASIC EXPLANATION -> Sending <<<<<<<<
# Reads self repo and combines to one big dump
# Figures out how much to send
# Transfers it back to the POC so it will send to new peer
#
# >>>>>>>> BASIC EXPLANATION -> Receiving <<<<<<<<
# Gets a dictionary of ranges recieved and data
# Sorts out the data by order (because 14'th chunk may come before 2'nd)
# Combines to one dump pile
# Saves it


class InitFilesHandle:

    # ----------------------- Writing

    # Functions takes all binary dumps sent from peers and saves them into the files
    # Params
    # repo: str -> directory path
    # binary_dict: dict -> Dictionary {(start_range, end_range): binary_data, (): b..., ...}
    @staticmethod
    def save_binary_repo_data(repo: str, binary_dict: dict) -> _void:
        # Binary arch
        # (In order)
        # NAM - Name of file
        # filename
        # FIL/DIR - Type of file
        # filecontent
        # \x00 - end of file (we chose)

        binary_dump = bytes()
        # Combine binaries into binary_dump
        # Sort by key
        for key, value in collections.OrderedDict(sorted(binary_dict.items())).items():
            binary_dump += value
            # key is not relevant if everything is sorted

        # Save them
        files_dump = binary_dump.split(chr(EOF_ASCII).encode())
        # filtering empty elements
        files_dump = list(filter(lambda el: el != b'', files_dump))
        total_repo_data = {}  # dict will be in format for FileDataHandle.write_to_files
        total_filenames = []
        for f in files_dump:
            data_dict = InitFilesHandle.extract_file_data(f)
            filename = data_dict['filename']
            total_filenames.append(filename)
            filevalue = data_dict['filecontent']
            total_repo_data[filename] = filevalue

        FileDataHandle.write_to_files(repo, total_repo_data)

        for filename in total_filenames:
            os.chmod(filename, 0o777)  # Give permissions to repo files

    # ----------------------- Reading

    @staticmethod
    def get_peer_binary_by_path(file_path: str, total_peers_number: int, pid: int):
        # Params
        # file_path: str -> The path which you want to copy
        # total_peers_number: int -> total peers in session
        # pid: int -> Your peer id
        # Returns
        # dict {<filename>: <3-byte-file-desc><filecontent>}

        # Function should be called once from each peer through a new connection
        # No connection made twice: either sent from HEAD or PEER

        # Size calculation
        path_dict = FileDataHandle.read_all_files(
            file_path)
        send_bytes = InitFilesHandle.convert_path_dict_to_binary_dump(
            path_dict)
        total_size = len(send_bytes)

        #
        addings = InitFilesHandle.divide_num_to_int_addings(
            total_size, total_peers_number)
        # Find starting send point
        start_loc = 0
        for i in range(pid):
            start_loc += addings[i]

        end_loc = start_loc + addings[pid]
        if end_loc > total_size:
            end_loc = total_size

        print(
            f'Sends: {start_loc} - {end_loc} | Total sent: {end_loc - start_loc} out of {total_size}')

        return Helper.add_zeros(total_size, MAX_REPO_BYTES_SIZE).encode() + Helper.populate_range_char((start_loc, end_loc), MAX_SENT_RANGE_LEN, RANGE_FILLER_CHAR).encode() + send_bytes[start_loc:end_loc]

    @staticmethod
    # Returns
    # size: int -> Size of files
    # path_dict: int -> To optimize, the data as well
    def calculate_binary_data_len(file_path: str):
        size = 0
        path_dict = FileDataHandle.read_all_files(file_path)
        for key, value in path_dict.items():
            # print(key, '-> ', value)
            content_type = value[0:3]
            if content_type == b'FIL':
                size += len(value[3:])
            elif content_type == b'DIR':
                size += InitFilesHandle.calculate_binary_data_len(
                    f'{file_path}{key}\\')[0]
        return size, path_dict

    @staticmethod
    def divide_num_to_int_addings(num: int, n: int):
        # Params - num -> number to divide, n -> amount of combinations
        # Returns - integer list with n elements which summed up equal to num
        left_to_add = num % n
        final = []
        for i in range(n):
            if i < left_to_add:
                final.append(num//n + 1)
            else:
                final.append(num//n)

        return final

    @staticmethod
    def convert_path_dict_to_binary_dump(path_dict: dict):
        dump = bytes()
        file_ending = chr(EOF_ASCII)
        for filename, value in path_dict.items():
            adding = 'NAM'.encode() + filename.encode() + base64.b64encode(value) + \
                file_ending.encode()  # Encode file data in base64
            dump += adding  # Encode all data to base64

        return dump

    # Function takes file dump (created by convert_path_dict_to_binary_dump) and extracts data to dict
    # Returns -> Dict suitable for FileDataHandle.write_to_files
    @staticmethod
    def extract_file_data(filedump: bytes):
        filedump = filedump[3:]  # NAM in the beginning is not necessary

        # Order in this loop matters
        # FIL can be in a dump with DIR
        # BUT
        # DIR should not be in the same dump with FIL

        if BASE64_DIR_ENCODED in filedump:
            # Since data is encoded in b64
            end_filename_loc = filedump.find(BASE64_DIR_ENCODED)
        else:
            end_filename_loc = filedump.find(BASE64_FIL_ENCODED)

        end_data_loc = len(filedump)  # EOF is not necssary

        return {
            'filename': filedump[:end_filename_loc].decode(),
            'filecontent': base64.b64decode(filedump[end_filename_loc:end_data_loc])
        }
