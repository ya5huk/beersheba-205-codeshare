import time
import socket
import threading

PEER_CONNECTION_CODE = '200'
SYNC_FILES_CODE = '201'
BACKUP_REQUEST_CODE = '202'


class Group:
    def __init__(self, group_name, DB, peer_pass):
        self.group_name = group_name
        self.DB = DB
        self.peer_pass = peer_pass
        self.current_pid = 0
        self.head_peer_sock = None
        self.head_peer_lock = threading.Lock()
        self.peers = {}  # elements will be: {PID: socket}

    def handle_peer(self, password):
        sock = None
        for i in range(10):
            if password in self.peer_pass:
                sock, address = self.peer_pass[password]
                del self.peer_pass[password]
                break
            time.sleep(1)

        if sock == None:
            return

        time.sleep(0.2)
        # Get details from user
        print('A new peer has connected with PID of: ' + str(self.current_pid))
        sock.send(f'{self.current_pid}'.encode())
        user_listening_port = sock.recv(1024).decode()

        if self.current_pid == 0:
            self.head_peer_sock = sock  # Set head peer if not yet set
            self.head_peer_lock.acquire()
            self.head_peer_sock.send(SYNC_FILES_CODE.encode())
            time.sleep(0.5)
            self.head_peer_sock.sendall(self.DB.get_group_files(
                self.group_name))  # Sync new peer over updates
            self.head_peer_sock.recv(1024)
            self.head_peer_lock.release()

            # Continuously ask for backups as long as only 1 peer is connected
            backups_thread = threading.Thread(target=self.request_backups)
            backups_thread.start()
            self.bup_thread = backups_thread

        # Add peer to peers list
        self.peers[self.current_pid] = sock

        # Inform the head peer that a new peer has connected
        if self.current_pid != 0:
            self.head_peer_lock.acquire()
            self.head_peer_sock.send((PEER_CONNECTION_CODE + str(
                self.current_pid) + ',' + address[0] + ',' + user_listening_port).encode())
            time.sleep(0.5)
            self.head_peer_lock.release()
        self.current_pid += 1

        while True:
            try:
                self.head_peer_lock.acquire()
                sock.settimeout(0.1)
                sock.recv(1)
                sock.settimeout(None)
                self.head_peer_lock.release()
                time.sleep(2)

            except socket.timeout as e:
                pass
                self.head_peer_lock.release()

            except Exception as e:
                # Delete from peers by sock
                pid = list(self.peers.keys())[
                    list(self.peers.values()).index(sock)]

                keys = list(self.peers.keys())
                values = list(self.peers.values())
                for index in range(keys.index(pid), len(keys) - 1):
                    self.peers[keys[index]] = values[index + 1]

                del self.peers[keys[-1]]

                self.current_pid -= 1
                print(f'{address} disconnected from the server.')

                # Reassign Head Peer if needed
                if pid == 0 and self.peers != {}:
                    self.head_peer_sock = self.peers[0]
                    print(
                        f'Assigned {self.peers[0].getpeername()} as new Head Peer')

                if len(self.peers) == 1:
                    if not self.bup_thread.is_alive():
                        backups_thread = threading.Thread(
                            target=self.request_backups, args=(self.group_name))
                        backups_thread.start()
                        self.bup_thread = backups_thread
                return

    def request_backups(self):
        print('Started backup')
        while len(self.peers) == 1:
            self.head_peer_lock.acquire()
            self.head_peer_sock.settimeout(None)
            self.head_peer_sock.send(BACKUP_REQUEST_CODE.encode())
            files = recvall(self.head_peer_sock)
            self.head_peer_lock.release()
            self.DB.set_group_files(files, self.group_name)
            time.sleep(60)  # Wait for next backup
        print('Finished backup')


def recvall(socket):
    BUFF_SIZE = 4096  # 4kb
    fragments = []
    while True:
        chunk = socket.recv(BUFF_SIZE)
        fragments.append(chunk)
        if len(chunk) < BUFF_SIZE:  # buffer is clear
            break
    return b''.join(fragments)
