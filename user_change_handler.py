import socket
import threading
import time
import random
import json

# This code is like a little POC that will blend with the POC code later.
# It may have multiple common lines with the current POC, but this code handles 
# only recieving and sending changes to the editor
# For recieving -> Print the code
# For sending changes -> editor will add them to current line

SPECIAL_END_CHAR = 'ך'

class ChangeHandler:
    def __init__(self, ip: str, port: int) -> None:
        self.port = port
        self.ip = ip
        self.client_threads = []

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind((ip, port))
        self.start_listening()


    def start_listening(self):
        self.sock.listen(5)
        print(f"Started listening on {self.ip} [{self.port}]")
        while True:
            conn, addr = self.sock.accept()
            print('Recieved connection', addr)
            
            # Listen to other user changes
            handlingThread = threading.Thread(target=self.handle_client_changes, args=(conn, addr))
            handlingThread.start()
            self.client_threads.append(handlingThread)

            # Send this user changes 
            sendingThread = threading.Thread(target=self.send_client_changes, args=(conn, addr))
            sendingThread.start()
            self.client_threads.append(sendingThread)

    @staticmethod
    def handle_client_changes(s: socket.socket, addr):
        print(f'Handling client -> {addr}')
        while True:
            change = s.recv(512)
            # Convert to dict
            change = json.loads(change.decode())
            print(change)

    @staticmethod
    def send_client_changes(s: socket.socket, addr):
        # Function will get info from the user and demonstrates how editor will get messages
        # For now I will simulate every 15 sec a random input (char) from this func
        random_inputs = [char for char in "Hey I want to buy a new suit and have no idea where... Could you recommend me a nice shop?"]
        
        delay = 5
        print(f'Started sending messages every {delay} seconds')
        while True:
            time.sleep(delay)
            random_index = random.randint(0, len(random_inputs)-1)
            random_line = random.randint(0, 10)
            random_col = random.randint(0, 40)
            msg_info = {
            'fl': 'smtth.txt',
            'ln': random_line + 1, # +1 because lines start from 1 and not 0
            'cl': random_col,
            'c': random_inputs[random_index]}
            s.send((json.dumps(msg_info)).encode()) # Just send dict as a string
            s.send(SPECIAL_END_CHAR.encode())
            print(f'Sent {random_inputs[random_index]} to {addr}')

def main():
    ChangeHandler('127.0.0.1', 4545)
    while True:
        pass

if __name__ == '__main__':
    main()
    