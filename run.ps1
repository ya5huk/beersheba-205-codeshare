

# Paths for desired files
$serverPath="MainServer.py"
$clientPath="POC.py"

# Input for client
$clientsNum = [int](Read-Host "Number of clients?")
$totalScreens = $clientsNum + 1
$topScreens = [int]($totalScreens / 2)
$bottomScreens = $totalScreens - $topScreens # Incase it is an odd number of screens

# Max screen reso
$maxHeight = (Get-Host).UI.RawUI.MaxWindowSize.Height
$maxWidth = (Get-Host).UI.RawUI.MaxWindowSize.Width

$topScreensWidth = $maxWidth / $topScreens
$bottomScreensWidth = $maxWidth / $bottomScreens


$screenHeight = $maxHeight / 2

# Start positioning
$currX = 0
$currY = 0

# Open Server
$serverCommand = -join("-noexit -command", " [console]::windowwidth=", $topScreensWidth,"; [console]::windowheight=", $screenHeight,";"," py ", $serverPath)
Start-Process powershell $serverCommand

# Open Clients

# Subtracted 1 because server is located at top and isn't a client
for ($i=0; $i -lt ($topScreens - 1); $i++) { # Cols
    $clientCommand = -join("-noexit -command "," [console]::windowwidth=", $topScreensWidth,"; [console]::windowheight=", $screenHeight,";", " py ", $clientPath)
    Start-Process powershell $clientCommand
    Start-Sleep 0.1 # To optimize the connetion (Not everyone at the same time)
}

# Start positioning
$currX = 0
$currY += $screenHeight

for ($i=0; $i -lt $bottomScreens; $i++) { # Cols
    $clientCommand = -join("-noexit -command "," [console]::windowwidth=", $bottomScreensWidth,"; [console]::windowheight=", $screenHeight,";", ";", " py ", $clientPath)
    Start-Process powershell $clientCommand
    Start-Sleep 0.1
}

Pause



