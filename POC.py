import base64
import json
from Files import FileSync, InitFilesHandle, Constants
from Files.FileDataHandle import FileDataHandle
from HelperFunctions import Helper
from pathlib import Path
import hashlib
import os
import pickle
import threading
import socket
from doctest import REPORT_CDIFF
import time
from re import S
import sys

IFH = InitFilesHandle.InitFilesHandle

PEER_CONNECTION_CODE = '200'
SYNC_FILES_CODE = '201'
BACKUP_REQUEST_CODE = '202'

ALLOWED = '100'

ALLOWED_HOST = '110'
ALLOWED_ADMIN = '111'
NOT_IN_GROUP = '112'
GROUP_DOESNT_EXISTS = '113'

WRONG_PASS = '101'
ALREADY_LOGGED_IN = '102'
USER_NOT_FOUND = '103'
NAME_ALREADY_EXISTS = '104'


# Init files transfer

REPO = 'repo/'

FIVE_KB_SIZE = 1024 * 5
MAX_PID_LEN = 5
MAX_REPO_BYTES_SIZE = 9
MAX_SENT_RANGE_LEN = 14
RANGE_FILLER_CHAR = 'x'

# main_is_local = input('Is main server located locally? y/n ') == 'y'
# if not main_is_local:
#     MAIN_SERVER_IP = input('Exact ip -> ')
# else:
#     MAIN_SERVER_IP = '127.0.0.1'

MAIN_SERVER_IP = '192.168.1.21'  # Should change for every run


def main():

    gui = GUI('127.0.0.1')
    gui.handle_client()


class GUI:
    def __init__(self, host):
        self.username = ''
        self.current_group = ''
        self.peer_thread = None
        self.is_intellij_session = False

        # Start Listening
        self.listen_sock = socket.socket(
            socket.AF_INET, socket.SOCK_STREAM)

        self.listen_sock.bind((host, 0))
        self.listen_sock.listen(5)
        port = self.listen_sock.getsockname()[1]
        print('Started listening for GUI on port: ' + str(port))

        self.gui_sock = None

        while not self.gui_sock:
            self.gui_sock, address = self.listen_sock.accept()
        print('Connected to GUI')

        self.client = Client()

    def handle_client(self):
        try:
            screen_to_handle = 'login'

            while True:
                if screen_to_handle == 'login':
                    screen_to_handle = self.login_handler(self.gui_sock)

                elif screen_to_handle == 'menu':
                    screen_to_handle = self.menu_handler(self.gui_sock)

                elif screen_to_handle == 'group':
                    screen_to_handle = self.group_handler(self.gui_sock)
        except Exception as e:
            print(e)
            self.client.main_server_sock.send('OUT'.encode())
            if self.client.peer:
                del self.client.peer
            if self.client.group_sock:
                self.client.group_sock.close()

            raise SystemExit(0)

    def login_handler(self, sock):
        while True:
            msg = sock.recv(1024).decode()
            msg_code = msg[:3]
            msg = msg[3:]
            print(msg)
            if msg_code == 'LOG':
                username, password = msg.split(',')
                ret_code = self.client.authenticate(username, password)

            elif msg_code == 'SUP':
                username, password = msg.split(',')
                ret_code = self.client.signup(username, password)

            elif msg_code == 'OUT':
                sock.close()

            sock.send(ret_code.encode())

            if ret_code == ALLOWED:
                self.username = username
                return 'menu'
            return 'login'

    def menu_handler(self, sock):
        while True:
            msg = sock.recv(1024).decode()
            msg_code = msg[:3]
            msg = msg[3:]

            if msg_code == 'CRE':
                group_name, repo = msg.split(',')
                ret_code = self.client.create_group(
                    group_name, repo, self.username)

                sock.send(ret_code.encode())

                if ret_code == ALLOWED:
                    self.current_group = group_name
                    return 'group'
                return 'menu'

            elif msg_code == 'ALL':
                groups = self.client.get_all_joined_groups(self.username)

                sock.send(groups.encode())

            elif msg_code == 'JOI':
                ret_code = self.client.join_group(msg, self.username)

                sock.send(ret_code.encode())

                if ret_code == ALLOWED or ALLOWED_ADMIN or ret_code == ALLOWED_HOST:
                    self.current_group = msg
                    return 'group'
                return 'menu'

            elif msg_code == 'AIN':
                invited_groups = self.client.get_all_invites(self.username)
                sock.send(invited_groups.encode())

            elif msg_code == 'ACC':
                self.client.main_server_sock.send(
                    ('ACC' + msg + ',' + self.username).encode())

            elif msg_code == 'REJ':
                self.client.main_server_sock.send(
                    ('REJ' + msg + ',' + self.username).encode())

            elif msg_code == 'OUT':
                sock.close()

    def group_handler(self, sock):
        while True:
            msg = sock.recv(1024).decode()
            msg_code = msg[:3]
            msg = msg[3:]
            if msg_code == 'ENT':
                repo = msg

                # Make the file if not exists
                Path(repo).mkdir(parents=True, exist_ok=True)

                if repo[-1] != '/' or repo[-1] != '\\':
                    repo += '/'

                self.client.main_server_sock.send(
                    ('ENT' + self.current_group + ',' + self.username).encode())
                password = self.client.main_server_sock.recv(1024).decode()

                ret_code = self.client.main_server_sock.recv(1024).decode()

                if ret_code == ALLOWED or ret_code == ALLOWED_ADMIN or ret_code == ALLOWED_HOST:
                    self.peer_thread = threading.Thread(target=self.client.log_to_group, args=(
                        password, repo), daemon=True)   # Talking thread
                    self.peer_thread.start()

            elif msg_code == 'LEA':
                if self.client.group_sock:
                    self.client.group_sock.close()
                time.sleep(1)
                if self.client.peer:
                    del self.client.peer
                    self.client.peer = None

            elif msg_code == 'INV':
                invited_user = msg
                ret_code = self.client.invite_to_group(
                    self.current_group, self.username, invited_user)

                sock.send(ret_code)

            elif msg_code == 'BAC':

                self.client.main_server_sock.send('BAC'.encode())
                self.current_group = ''
                return 'menu'

            elif msg_code == 'EDI':
                ithread = threading.Thread(target=self.client.start_intellij)
                ithread.start()

            elif msg_code == 'SYN':
                ithread = threading.Thread(target=self.client.restart_sync)
                ithread.start()

            elif msg_code == 'OUT':
                sock.close()


class Client:
    def __init__(self):
        # Main server info
        self.server_ip = MAIN_SERVER_IP
        self.server_port = 8081
        self.group_server_port = 8082
        self.group_sock = None
        self.peer = None

        # Connect to main server
        self.main_server_sock = socket.socket(
            socket.AF_INET, socket.SOCK_STREAM)

        server_address = (self.server_ip, self.server_port)
        self.main_server_sock.connect(server_address)

    def authenticate(self, username, password):
        hashed_pass = hashlib.md5(password.encode()).hexdigest()
        self.main_server_sock.send(
            ('LOG' + username + ',' + hashed_pass).encode())
        return self.main_server_sock.recv(1024).decode()

    def signup(self, username, password):
        hashed_pass = hashlib.md5(password.encode()).hexdigest()
        self.main_server_sock.send(
            ('SUP' + username + ',' + hashed_pass).encode())
        return self.main_server_sock.recv(1024).decode()

    def logout(self, username):
        self.main_server_sock.send(('OUT' + username).encode())

    def create_group(self, group_name, repo, host_name):
        Path(repo).mkdir(parents=True, exist_ok=True)

        if repo[-1] != '/' or repo[-1] != '\\':
            repo += '/'
        files = pickle.dumps(read_all_files(repo))

        self.main_server_sock.send(
            ('CRE' + group_name + ',' + host_name).encode())
        self.main_server_sock.sendall(files)
        return self.main_server_sock.recv(1024).decode()

    def join_group(self, group_name, user):
        self.main_server_sock.send(('JOI' + group_name + ',' + user).encode())
        return self.main_server_sock.recv(1024).decode()

    def get_all_joined_groups(self, username):
        self.main_server_sock.send(('ALL' + username).encode())
        return self.main_server_sock.recv(1024).decode()

    def invite_to_group(self, group_name, inviting_user, invited_user):
        self.main_server_sock.send(
            ('INV' + group_name + ',' + inviting_user + ',' + invited_user).encode())
        return self.main_server_sock.recv(1024)

    def get_all_invites(self, username):
        self.main_server_sock.send(('AIN' + username).encode())
        return self.main_server_sock.recv(1024).decode()

    def log_to_group(self, password, repo):

        # Connect to group server
        self.group_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        server_address = (self.server_ip, self.group_server_port)
        self.group_sock.connect(server_address)

        self.group_sock.send(password.encode())

        # Get your PID and listening port from server
        pid = int(self.group_sock.recv(1024).decode())

        print('PID is: ', pid)

        self.peer = Peer(pid, repo, self.group_sock)
        self.peer.start()

    def start_intellij(self):
        temp_path = os.path.expandvars(R"${TEMP}")
        if not os.path.exists(temp_path + '\\' + 'LiveCodeShare'):  # To save temp files
            os.mkdir(temp_path + '\\' + 'LiveCodeShare')

        print(f'Opening intellij idea')
        if not self.peer:
            print(f'Cant open. Peer is {self.peer}')

        self.is_intellij_session = True
        if self.peer:
            p = self.peer
            p.is_syncing = False

            # Start a new sock to intellij
            p.intellij_listening_sock = self.listen_sock = socket.socket(
                socket.AF_INET, socket.SOCK_STREAM)
            # OS chooses open port automatically
            p.intellij_listening_sock.bind(('', 0))
            p.intellij_listening_sock.listen(1)
            print('Listening to intellij incoming connection at -> ',
                  p.intellij_listening_sock.getsockname())

            # Pass port to intellij through temporary file

            fullpath = temp_path + '\\' + 'LiveCodeShare\\'

            # Create a file at Temp folder named by port intellij should connect to
            # NOTE intellij will know how to connect by checking last file that was created (to test 2 users on one computer)
            filepath = f'{fullpath}{p.intellij_listening_sock.getsockname()[1]}'
            print(f'Created file for intellij to recognize POC -> {filepath}')
            Path(filepath).touch(exist_ok=True, mode=0o777)

            # Start vscode
            os.system(f'code {p.repo}')
            intellij_thread = threading.Thread(
                target=p.communicate_with_intellij)
            intellij_thread.start()

    def restart_sync(self):
        self.is_intellij_session = False
        if self.peer:
            p = self.peer
            p.is_syncing = True


class Peer:
    def __init__(self, pid, repo, sock=None):
        self.repo = repo
        self.pid = pid
        self.main_server_sock = sock
        self.removed_users_buffer = []
        self.shutdown = False
        self.init_files_lock = threading.Lock()
        self.repo_listener = None
        self.is_syncing = True
        self.intellij_listening_sock = None
        self.intellij_communication_sock = None

    def communicate_with_intellij(self):
        self.intellij_communication_sock, addr = self.intellij_listening_sock.accept()
        print(f'Peer has connected to intellij ({addr})')
        while True:
            if not self.intellij_communication_sock:
                print('Sock disconnected between Intellij editor and POC')
                return

            msg = self.intellij_communication_sock.recv(516).decode()

            # Echo messages
            for sock in self.connected_peers.values():
                sock.send(('EDU' + msg).encode())

    def start(self):
        # Create a new socket to listen on
        self.connected_peers = {}  # elements will be: {PID: socket}

        self.listen_sock = socket.socket(
            socket.AF_INET, socket.SOCK_STREAM)

        self.listen_sock.bind(('', 0))  # OS chooses open port automatically

        port = self.listen_sock.getsockname()[1]
        self.listen_sock.listen(5)
        time.sleep(1)
        self.main_server_sock.send(str(port).encode())

        # Check if the peer is the head peer
        if self.pid == 0:
            print('Head Peer')
            # Head Peer thread
            main_server_thread = threading.Thread(
                target=self.communicate_with_main_server, daemon=True)
            main_server_thread.start()
        else:
            print('Peer')

        print('Listening on port:' + str(port))

        # Peers listening thread
        listener_thread = threading.Thread(
            target=self.listen_to_new_peers, daemon=True)  # Listening thread
        listener_thread.start()

        # Peers messaging thread
        messaging_thread = threading.Thread(
            target=self.talk_with_peers, daemon=True)   # Talking thread
        messaging_thread.start()

        while not self.shutdown:
            time.sleep(1)

    def communicate_with_main_server(self):
        print('Started Communication With Main Server')

        try:
            while True:
                # Get message from main server (Newly connecting peers' info, Files updates, Backup requests)
                msg = self.main_server_sock.recv(1024).decode()
                msg_code = msg[:3]

                # New peer
                if msg_code == PEER_CONNECTION_CODE:
                    msg = msg[3:]
                    pid, ip, port = msg.split(',')

                    # Connect to new peer
                    sock = socket.socket(
                        socket.AF_INET, socket.SOCK_STREAM)

                    server_address = (ip, int(port))
                    sock.connect(server_address)

                    self.connected_peers[pid] = sock
                    try:
                        pass
                        # print(IFH.get_peer_binary_by_path(
                        #     self.repo, len(self.connected_peers), self.pid))
                        sock.send(
                            f'{add_zeros(self.pid, MAX_PID_LEN)}\n\n\n'.encode() + IFH.get_peer_binary_by_path(self.repo, len(self.connected_peers), self.pid))
                    except Exception as e:
                        print('Failed sending my binary repo part -> ', e)

                    print('A new peer has connected with PID (to HEAD): ' + pid)

                    # Create a thread to handle communication with the new peer
                    client_thread = threading.Thread(target=self.handle_peers, args=(
                        pid, sock, server_address), daemon=True)  # Start a new thread for each peer
                    client_thread.start()

                    self.repo_listener.add_changes_socket(sock)

                    # Inform all the other peers that a new peer has connected
                    for peer in self.connected_peers.values():
                        if peer != sock:
                            peer.send(
                                ('CON,' + pid + ',' + ip + ',' + port).encode())

                # Sync files
                elif msg_code == SYNC_FILES_CODE:
                    time.sleep(2)  # Let client send all files
                    pickled_files = recvall(self.main_server_sock)
                    files = pickle.loads(pickled_files)
                    write_to_files(self.repo, files)
                    self.main_server_sock.send('100'.encode())

                    # Start watching when files are downloaded
                    if self.repo_listener is None:
                        self.start_repo_listener()

                # Backup files
                elif msg_code == BACKUP_REQUEST_CODE:
                    files_dict = read_all_files(self.repo)
                    pickled_files = pickle.dumps(files_dict)
                    self.main_server_sock.sendall(pickled_files)

        except Exception as e:
            print('Exception occured -> ', e)
            self.shutdown = True

    def talk_with_peers(self):
        while not self.shutdown:
            msg = input()
            for sock in self.connected_peers.values():
                sock.send(('MSG' + msg).encode())

    def listen_to_new_peers(self):
        # Wait for new connections
        # dict of key->value = (start, end)->binary_data
        repo_data_received = {}
        repo_size = 0  # In bytes
        data_bytes_received = 0
        while not self.shutdown:
            sock, address = self.listen_sock.accept()

            # Size:       5 bits 3 bits      8 bits                  14 bits                       chunks of 4096
            # Protocol is <pid><\n\n\n><8-digit-full-repo-size><14-digit-sent_data_binary_range><binary_repo_data>
            pid = sock.recv(MAX_PID_LEN).decode()
            # Locking data received resource
            self.init_files_lock.acquire(blocking=True)
            try:

                if (sock.recv(3).decode() == '\n\n\n'):
                    repo_size = int(sock.recv(MAX_REPO_BYTES_SIZE).decode())
                    range_received = Helper.decode_range_str(
                        sock.recv(MAX_SENT_RANGE_LEN).decode(), RANGE_FILLER_CHAR)
                    print('received file content from: ', address)

                    # file content
                    # the only reason I split it like this, so we can get total loaded bytes
                    new_repo_data_received = sock.recv(
                        range_received[1] - range_received[0])

                    repo_data_received[range_received] = new_repo_data_received
                    data_bytes_received += len(new_repo_data_received)

                    print(f'Received {data_bytes_received} / {repo_size}')
            except Exception as e:
                print('Error while getting init files: ', e)

            try:
                if repo_size <= data_bytes_received:  # Only if all data was recieved try to save file
                    IFH.save_binary_repo_data(self.repo, repo_data_received)
                    # Optional - equalize hashes of filse from server or head
                    print('Init files were saved successfully.')
            except Exception as e:
                print('Error saving init files -> ', e)
            finally:
                self.init_files_lock.release()  # Only now files should have been saved

            self.connected_peers[pid] = sock

            print('A new peer has connected with PID (from listening): ' + pid)

            # After saving init files, start listening to repo changes
            # Repo listening
            try:
                if self.repo_listener is None:
                    self.start_repo_listener()
                    time.sleep(0.1)
                self.repo_listener.add_changes_socket(sock)
            except Exception as e:
                print('Failed starting repo watch:', e)
            # Start a new thread for to handle each peer
            client_thread = threading.Thread(target=self.handle_peers, args=(
                pid, sock, address), daemon=True)
            client_thread.start()

    def start_repo_listener(self):
        os.startfile(os.path.realpath(self.repo))
        self.repo_listener = FileSync.RepoListener(repo=self.repo)
        repo_listening_thread = threading.Thread(
            target=self.repo_listener.listen_to_repo_changes)
        repo_listening_thread.start()

    def handle_peers(self, pid, sock, address):

        try:
            while not self.shutdown:
                # If own pid in connected_peers by sock isnt pid change pid
                if list(self.connected_peers.keys())[list(self.connected_peers.values()).index(sock)] != pid:
                    pid = list(self.connected_peers.keys())[
                        list(self.connected_peers.values()).index(sock)]

                msg = sock.recv(Constants.ONE_MEGABYTE).decode()
                print(msg)
                # Message
                if msg[0:3] == 'MSG':
                    print(str(address) + ': ' + msg[3:])

                # Head Peer telling to connect to a new peer
                elif msg[0:3] == 'CON':
                    new_pid, ip, port = msg.split(',')[1:]

                    # Connect to the new peer
                    new_sock = socket.socket(
                        socket.AF_INET, socket.SOCK_STREAM)

                    server_address = (ip, int(port))
                    new_sock.connect(server_address)

                    try:
                        greeting_msg = bytes()  # Includes pid send to new comer + file data
                        greeting_msg += f'{add_zeros(self.pid, MAX_PID_LEN)}\n\n\n'.encode()
                        if (int(new_pid) > int(self.pid)):  # Update only new comer with file
                            greeting_msg += IFH.get_peer_binary_by_path(
                                self.repo, len(self.connected_peers) + 1, self.pid)
                            # Added 1 to connceted peers len because peer does not count himself
                        new_sock.send(greeting_msg)
                    except Exception as e:
                        print('Error sending my repo binary -> ', e)

                    try:
                        self.repo_listener.add_changes_socket(sock)
                    except Exception as e:
                        print('Error adding sending socket to repo_listener -> ', e)

                    self.connected_peers[new_pid] = new_sock

                    print(
                        f'A new peer has connected with PID (info from HEAD): ' + new_pid)

                    # Create a thread to handle communication with the new peer
                    client_thread = threading.Thread(target=self.handle_peers, args=(
                        pid, new_sock, server_address), daemon=True)
                    client_thread.start()

                elif msg[0:3] == 'UPD':
                    if not self.is_syncing:  # Block messages if not syncing
                        continue
                    try:
                        additions, changes, deletions = parse_update_msg(msg)
                        FileDataHandle.force_directory_update(self.repo_listener, self.repo,
                                                              additions, changes, deletions)
                    except Exception as e:
                        print(
                            f'Could not force update on dir {self.repo} -> ', e)

                elif msg[:3] == 'EDU':
                    if self.intellij_communication_sock:
                        print(
                            f'Passing {msg[3:]} to intellij communication sock')
                        self.intellij_communication_sock.send(msg[3:].encode())

        except Exception as e:
            lowered_pid = False
            # If own pid in connected_peers by sock isnt pid change pid
            if list(self.connected_peers.keys())[list(self.connected_peers.values()).index(sock)] != pid:
                pid = list(self.connected_peers.keys())[
                    list(self.connected_peers.values()).index(sock)]

            if int(pid) < self.pid:
                self.pid -= 1
                lowered_pid = True

            print('Connection lost with: ' +
                  str(address) + ', PID is now:', self.pid)

            # Decrease all PID's above the disconnected one
            keys = list(self.connected_peers.keys())
            values = list(self.connected_peers.values())
            for index in range(keys.index(pid), len(keys) - 1):
                self.connected_peers[keys[index]] = values[index + 1]

            del self.connected_peers[keys[-1]]

            if lowered_pid and self.pid == 0:
                print('Reassigned as Head Peer')
                main_server_thread = threading.Thread(
                    target=self.communicate_with_main_server, daemon=True)
                main_server_thread.start()

            lowered_pid = False


def write_to_files(path, files):
    Path(path).mkdir(parents=True, exist_ok=True)
    for filename, file_content in files.items():
        # Sync directory recursively
        if file_content[:3] == b'DIR':
            dir_files = pickle.loads(file_content[3:])
            write_to_files(path + filename + '/', dir_files)

        elif file_content[:3] == b'FIL':
            f = open(path + filename, "wb")
            f.write(file_content[3:])
            f.close()

# Function returns a dictionary of all files and their contents in a dirctory, also all files in subdirs are saved as pickled values of the directory's key


def read_all_files(path):
    file_dict = {}
    files = os.listdir(path)
    for file in files:
        if os.path.isdir(path + file):
            file_dict[file] = b'DIR' + \
                pickle.dumps(read_all_files(path + file + '/'))
        else:
            f = open(path + file, 'rb')
            file_content = f.read()
            f.close()
            file_dict[file] = b'FIL' + file_content
    return file_dict


def recvall(socket):
    BUFF_SIZE = 4096  # 4kb
    fragments = []
    while True:
        chunk = socket.recv(BUFF_SIZE)
        fragments.append(chunk)
        if len(chunk) < BUFF_SIZE:  # buffer is clear
            break
    return b''.join(fragments)


def add_zeros(num: int, zeros: int):
    if len(str(num)) > zeros:
        raise Exception(f'Number {num} len must be shorter than {zeros}')
    if zeros < 0 or num < 0:
        raise Exception(f'Zeros number or number must be above 0')
    return '0'*(zeros - len(str(num))) + str(num)


def parse_update_msg(msg: str):
    start_of_additions_index = Constants.PROTOCOL_CODE_LEN+Constants.MAX_ADDITIONS_LEN

    additions_len = int(
        msg[Constants.PROTOCOL_CODE_LEN:start_of_additions_index])
    additions = msg[start_of_additions_index:
                    start_of_additions_index+additions_len]

    changes_len = int(msg[start_of_additions_index +
                          additions_len:start_of_additions_index + additions_len + Constants.MAX_CHANGES_LEN])
    start_of_changes_index = start_of_additions_index + \
        additions_len + Constants.MAX_CHANGES_LEN
    changes = msg[start_of_changes_index:start_of_changes_index + changes_len]

    deletions_len = int(
        msg[start_of_changes_index+changes_len:start_of_changes_index+changes_len+Constants.MAX_DELETIONS_LEN])
    deletions = msg[len(
        msg) - deletions_len:].split(Constants.DELETED_FILES_SEPERATOR)

    return json.loads(additions), json.loads(changes), deletions


if __name__ == '__main__':
    main()
