package org.jetbrains.plugins.template.extensions

import com.intellij.codeInsight.editorActions.TypedHandlerDelegate
import com.intellij.execution.filters.TextConsoleBuilderFactory
import com.intellij.execution.ui.ConsoleViewContentType
import com.intellij.openapi.command.WriteCommandAction
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.fileEditor.FileDocumentManager
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.project.ProjectManager
import com.intellij.openapi.util.text.StringUtil
import com.intellij.openapi.wm.ToolWindowManager
import com.intellij.psi.PsiFile
import org.json.JSONObject
import java.io.BufferedReader
import java.io.File
import java.net.Socket
import java.util.*

const val IP = "127.0.0.1"
const val SPECIAL_STOP_CHAR_CODE = 1498
val tempDirPath = "${System.getenv("LOCALAPPDATA")}\\Temp\\LiveCodeShare\\"
var files: Array<File> = File(tempDirPath).listFiles()!!

var currProject = ProjectManager.getInstance().defaultProject

//// Just to preview errors
//val toolWindow = ToolWindowManager.getInstance(currProject).getToolWindow("PluginOutput");
//val consoleView = TextConsoleBuilderFactory.getInstance().createBuilder(currProject).console;
//val content = toolWindow?.contentManager?.factory?.createContent(consoleView.component, "Plugin Output", false);

fun receiveChanges(br: BufferedReader) : String {
    var currStr = ""
    // Read one message
    do {
        val ch = br.read()
        if(ch == SPECIAL_STOP_CHAR_CODE) //
            break
        currStr += ch.toChar()

    }  while (true)

    return currStr
}

var charOutputQueue: Queue<Char> = LinkedList()
// Well it is java... Don't ask
// That's how you thread
// Another option is async, should consider

// Note:
// A lot of time we pass object to a thread, that's currently only for print
class CharReceiveThread: Thread {
    private var sock: Socket

    constructor(sock: Socket): super(Thread()) {
        this.sock = sock
    }

    private fun insertEditor(filename: String, line: Int, col: Int, change: String) {
        val manager = FileEditorManager.getInstance(currProject)
        val editor = manager.allEditors[0]
        if (editor.file?.name == filename) {
            println("File $filename found in one of the editors")
            println("Placed $change at ln $line, cl $col")
            var document = FileDocumentManager.getInstance().getDocument(editor.file!!)
            WriteCommandAction.runWriteCommandAction(currProject) {
                var offset = document?.getLineStartOffset(line)?.plus(col)
                document?.insertString(offset!!, change)

            }
        //document?.insertString(5, change)
            //val offset = AbstractValueHint.calculateOffset(Editor, Point(line, col))
            //editor.document.insertString(offset, change)
        } else {
            println("File is not opened, changing data of file:")
            // editor.file ...
            // TODO: change the file content without displaying
        }



        }



    override fun run() {
        val br = this.sock.getInputStream().bufferedReader()

        while (true) {
            try {
                val currStr = receiveChanges(br)
                println("Recv $currStr")
                val obj = JSONObject(currStr)

            val file = obj["fl"].toString()
            val line = obj["ln"].toString().toInt()
            val col = obj["cl"].toString().toInt()
            val change = obj["c"].toString()


            insertEditor(file, line, col, change)
            }
            catch(e: Exception) {
                println(e)
            }
        }

    }
}

class HandleCharTypeThread : Thread {
    private var sock: Socket? = null

    constructor(sock: Socket) : super(Thread()) {
        this.sock = sock
    }

    override fun run() {
        if (charOutputQueue.isEmpty()) {
            this.run()
        }

        // Only if queue is not empty
        var c = charOutputQueue.remove()
        this.sock?.getOutputStream()?.write(c.toString().toByteArray())
    }
}

fun getSocket() : Socket {
    // Find connecting (intellij <---> POC) file located in Temp
    // tempDirPath refreshes for every run

    var newestTimestamp = files[0].lastModified();
    var newestFilename = files[0].name;
    var port = -1

    println("Cycling through $files")

    files.forEach{
        val currTimestamp = it.lastModified()
        if (currTimestamp > newestTimestamp) {
            // Tackled currently the newest file
            newestTimestamp = currTimestamp
            newestFilename = it.name
        }
    }

    try {
        port = newestFilename.toInt() // Newest file will be named the port intellij should connect to
        println("Found address $IP:$port")
        Thread.sleep(10_000)
    }
    catch (e: Exception) {
//            consoleView.print("Error -> $e", ConsoleViewContentType.NORMAL_OUTPUT);
    }

//            consoleView.print("Connected to $IP:$port!", ConsoleViewContentType.NORMAL_OUTPUT);
    return Socket(IP, port)

}

class TypeHandler : TypedHandlerDelegate() {

    private var sock: Socket = getSocket();

    init {
        println("Started TypeHandler")
        // Setting up console to print
//        toolWindow?.contentManager?.addContent(content!!);




//        consoleView.print("Found address $IP:$port", ConsoleViewContentType.NORMAL_OUTPUT);





        // Thread will receive info
        val receiveTh = CharReceiveThread(sock)
        receiveTh.start()

        // Thread will send info
        // Currently bugged:
//        val sendTh = HandleCharTypeThread(sock, currProject)
//        sendTh.start()
    }

    override fun charTyped(c: Char, project: Project, editor: Editor, file: PsiFile): Result {
        // charOutputQueue.add(c) Currently bugged
        currProject = project

        val lineColumn = StringUtil.offsetToLineColumn(file.text, editor.caretModel.offset)
        val col = lineColumn.column
        val line = lineColumn.line
        val obj = JSONObject()
        // TODO: Get real file name
        obj.put("fl", file.name)
        obj.put("ln", line)
        obj.put("cl", col)
        obj.put("c", c)
        this.sock.getOutputStream()?.write(obj.toString().toByteArray())
        // TODO: Delay the send for every 300ms
        // Currently works fine without delay
        return Result.STOP
    }


}

