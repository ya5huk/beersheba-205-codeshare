package org.jetbrains.plugins.template.actions

import com.intellij.notification.NotificationGroupManager
import com.intellij.notification.NotificationType
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.editor.actionSystem.TypedActionHandler

class UserChange : AnAction() {
    override fun update(e: AnActionEvent) {
        val project = e.project
        val editor = e.getData(CommonDataKeys.EDITOR)

        if (editor != null && project != null) {
            // Ensure the list of carets in the editor is not empty
            NotificationGroupManager.getInstance().getNotificationGroup("Preferences notifications")
                .createNotification("$editor", NotificationType.INFORMATION)
                .notify(e.project);
        }

    }

    override fun actionPerformed(e: AnActionEvent) {
        TODO("Not yet implemented")
    }
}