package org.jetbrains.plugins.template.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.ui.Messages;
import com.intellij.notification.NotificationGroupManager
import com.intellij.notification.NotificationType
import com.intellij.openapi.project.Project


public var othersMouseVisibility = true;
public var soloCompilation = false;

class ToggleMouseVisibilityAction : AnAction() {
    // Runs at start of the action
    override fun actionPerformed(e: AnActionEvent) {
        othersMouseVisibility = !othersMouseVisibility;
        var visibilityMsg = if (othersMouseVisibility) "visible" else "not visible"
        NotificationGroupManager.getInstance().getNotificationGroup("Preferences notifications")
            .createNotification("Others' mouse is now $visibilityMsg", NotificationType.INFORMATION)
            .notify(e.project);

    }

    // Runs every update
    override fun update(e: AnActionEvent) {
        println("Update happened")
    }

}
class ToggleSoloCompilation : AnAction() {
    // Runs at start of the action
    override fun actionPerformed(e: AnActionEvent) {
        soloCompilation = !soloCompilation;
        var compilationMsg= if (soloCompilation) "Your" else "Others'"
        NotificationGroupManager.getInstance().getNotificationGroup("Preferences notifications")
            .createNotification("$compilationMsg code is now set for compilation", NotificationType.INFORMATION)
            .notify(e.project);

    }

    // Runs every update
    override fun update(e: AnActionEvent) {
        println("Update happened")
    }

}
