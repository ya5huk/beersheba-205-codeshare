# Beersheba-205-CodeShare
An app to share and edit together.

## Quick Start

Copy from git

`git clone https://gitlab.com/ya5huk/beersheba-205-codeshare/`

Run Main Server to configure new connections

`py MainServer.py`

For each account you need to:

1. Start graphics -> `"Start graphics".exe` (in the main folder)
2. Start client -> `py POC.py`


## Run program

The instructions for connection between GUI frontend to backend will appear on the screen.

## About

- The app's client is made with Microsoft WPF
- Peer to Peer implemented in Python
- Main server & Client backend written in Python
- VSCode extension written in Javascript
