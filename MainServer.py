from asyncio.windows_utils import BUFSIZE
import time
import socket
import threading
import random
from Group import Group

from DBHandler import DBHandler

PEER_CONNECTION_CODE = '200'
SYNC_FILES_CODE = '201'
BACKUP_REQUEST_CODE = '202'

ALLOWED = '100'

ALLOWED_HOST = '110'
ALLOWED_ADMIN = '111'
NOT_IN_GROUP = '112'
GROUP_DOESNT_EXISTS = '113'

WRONG_PASS = '101'
ALREADY_LOGGED_IN = '102'
USER_NOT_FOUND = '103'
NAME_ALREADY_EXISTS = '104'


def main():

    main_server = MainServer('0.0.0.0')


class MainServer:
    def __init__(self, host, port=8081):
        self.groups = {}
        self.peer_pass = {}
        self.head_peer_lock = threading.Lock()
        # Currently hard coded, when we'll have authentication and GUI user will choose which group to work on
        self.group_id = '1'
        self.current_pid = 0
        self.head_peer_sock = None
        self.peers = {}  # elements will be: {PID: socket}

        # Database initializing
        self.DB = DBHandler('database.db')

        self.DB.create_tables()

        peers_thread = threading.Thread(
            target=self.listen_to_peers, args=(host, ))
        peers_thread.start()

        # Start Listening
        self.listen_sock = socket.socket(
            socket.AF_INET, socket.SOCK_STREAM)

        self.listen_sock.bind((host, port))
        self.listen_sock.listen(5)

        print('Main server listening on port', port)

        g = Group('cool', self.DB, self.peer_pass)

        while True:
            # For each connecting peer
            sock, address = self.listen_sock.accept()
            print('A new client has connected')

            client_thread = threading.Thread(target=self.handle_client, args=(
                sock, address))
            client_thread.start()

    def listen_to_peers(self, host, port=8082):
        # Start Listening
        self.peer_listen_sock = socket.socket(
            socket.AF_INET, socket.SOCK_STREAM)

        self.peer_listen_sock.bind((host, port))
        self.peer_listen_sock.listen(5)

        while True:
            # For each connecting peer
            sock, address = self.peer_listen_sock.accept()

            authentication_thread = threading.Thread(target=self.authenticate_peer, args=(
                sock, address))
            authentication_thread.start()

    def authenticate_peer(self, sock, address):
        try:
            password = sock.recv(1024).decode()
            self.peer_pass[password] = (sock, address)
            time.sleep(10)
            if password in self.peer_pass:
                del self.peer_pass[password]

        except Exception as e:
            pass

    def handle_client(self, sock, address):

        username = ''
        screen_to_handle = 'login'
        try:
            while True:
                if screen_to_handle == 'login':
                    screen_to_handle, username = self.handle_login(sock)

                elif screen_to_handle == 'menu':
                    screen_to_handle = self.handle_menu(sock)

                elif screen_to_handle == 'group':
                    screen_to_handle = self.handle_group(sock, address)

                elif screen_to_handle == 'out':
                    if username != '':
                        print(username + ' Logged out')
                        self.DB.logout(username)
                    break

        except Exception as e:
            print(e)

    def handle_login(self, sock):
        while True:
            msg = sock.recv(1024).decode()
            msg_code = msg[:3]
            msg = msg[3:]

            # Login
            if msg_code == 'LOG':
                username, password = msg.split(',')
                ret_code = self.DB.login(username, password)

            # Sign up
            elif msg_code == 'SUP':
                username, password = msg.split(',')
                ret_code = self.DB.signup(username, password)

            elif msg_code == 'OUT':
                return 'out', ''

            sock.send(ret_code.encode())

            if ret_code == ALLOWED:
                return 'menu', username
            return 'login', ''

    def handle_menu(self, sock):
        while True:
            ret_code = ''
            msg = sock.recv(1024).decode()
            msg_code = msg[:3]
            msg = msg[3:]

            if msg_code == 'CRE':
                group_name, host_name = msg.split(',')
                files = recvall(sock)
                ret_code = self.DB.create_group(group_name, host_name, files)

            elif msg_code == 'JOI':
                group_name, username = msg.split(',')
                ret_code = self.DB.join_group(group_name, username)

            elif msg_code == 'ALL':
                ret_code = self.DB.get_all_joined_groups(msg)

            elif msg_code == 'AIN':
                ret_code = self.DB.get_all_invites(msg)

            elif msg_code == 'ACC':
                group_name, username = msg.split(',')
                self.DB.accept_invite(group_name, username)

            elif msg_code == 'REJ':
                group_name, username = msg.split(',')
                self.DB.reject_invite(group_name, username)

            elif msg_code == 'OUT':
                return 'out'

            if ret_code != '':
                sock.send(ret_code.encode())

            if ret_code == ALLOWED or ret_code == ALLOWED_ADMIN or ret_code == ALLOWED_HOST:
                return 'group'
            return 'menu'

    def handle_group(self, sock, address):
        while True:
            msg = sock.recv(1024).decode()
            msg_code = msg[:3]
            msg = msg[3:]
            if msg_code == 'ENT':
                group_name, username = msg.split(',')
                ret_code = self.DB.join_group(group_name, username)
                if ret_code == ALLOWED or ret_code == ALLOWED_ADMIN or ret_code == ALLOWED_HOST:
                    # Peer thread
                    if group_name not in self.groups:
                        self.groups[group_name] = Group(
                            group_name, self.DB, self.peer_pass)
                    password = str(random.randint(1000000, 9999999))
                    sock.send(password.encode())
                    peer_thread = threading.Thread(
                        target=self.groups[group_name].handle_peer, args=(password, ))   # Peer thread
                    peer_thread.start()

                sock.send(ret_code.encode())

            elif msg_code == 'INV':
                group_name, inviting_user, invited_user = msg.split(',')
                ret_code = self.DB.invite_to_group(
                    group_name, inviting_user, invited_user)
                sock.send(ret_code.encode())

            elif msg_code == 'BAC':
                return 'menu'

            elif msg_code == 'OUT':
                return 'out'


def recvall(socket):
    BUFF_SIZE = 4096  # 4kb
    fragments = []
    while True:
        chunk = socket.recv(BUFF_SIZE)
        fragments.append(chunk)
        if len(chunk) < BUFF_SIZE:  # buffer is clear
            break
    return b''.join(fragments)


if __name__ == '__main__':
    main()
