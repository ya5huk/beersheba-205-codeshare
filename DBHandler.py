import sqlite3
import pickle

ALLOWED = '100'

ALLOWED_HOST = '110'
ALLOWED_ADMIN = '111'
NOT_IN_GROUP = '112'
GROUP_DOESNT_EXISTS = '113'
INSUFFICIENT_PERMS = '114'

WRONG_PASS = '101'
ALREADY_LOGGED_IN = '102'
USER_NOT_FOUND = '103'
NAME_ALREADY_EXISTS = '104'



class DBHandler:
    def __init__(self, db_file):
        # Database initializing 
        self.con = sqlite3.connect(db_file, check_same_thread=False)


    def create_tables(self):
        # Create table
        cur = self.con.cursor()
        cur.execute('CREATE TABLE IF NOT EXISTS groups (name text PRIMARY KEY, host text, admins BLOB, participants BLOB, files BLOB, invites BLOB);')
        cur.execute('CREATE TABLE IF NOT EXISTS users (name text PRIMARY KEY, password text, is_logged text);')
        cur.execute('UPDATE users SET is_logged = "False";')
        self.con.commit()


    def login(self, username, password):
        # Get user
        cur = self.con.cursor()
        rows = cur.execute('SELECT * FROM users WHERE name = ?;', (username,))
        rows = cur.fetchall()

        # Check if passwords match
        if len(rows) > 0:
            if len(rows[0]) > 0:
                if rows[0][1] == password:
                    if rows[0][2] == 'False':
                        cur.execute('UPDATE users SET is_logged = "True" WHERE name = ?;', (username, ))
                        self.con.commit()
                        return ALLOWED
                    else:
                        return ALREADY_LOGGED_IN
                else:
                    return WRONG_PASS
        return USER_NOT_FOUND

    def signup(self, username, password):
        # Check if user exists
        cur = self.con.cursor()
        cur.execute('SELECT * FROM users WHERE name = ?;', (username,))
        rows = cur.fetchall()

        if len(rows) > 0:
            if len(rows[0]) > 0:
                return NAME_ALREADY_EXISTS

        cur.execute('INSERT INTO users (name, password, is_logged) VALUES (?, ?, ?);', (username, password, 'True'))
        self.con.commit()

        return ALLOWED

    def logout(self, username):
        cur = self.con.cursor()
        cur.execute('UPDATE users SET is_logged = "False" WHERE name = ?;', (username,))
        self.con.commit()

    def create_group(self, group_name, host_name, files):
        # Check if group exists
        cur = self.con.cursor()
        cur.execute('SELECT * FROM groups WHERE name = ?;', (group_name,))
        rows = cur.fetchall()

        if len(rows) > 0:
            if len(rows[0]) > 0:
                return NAME_ALREADY_EXISTS

        cur.execute('INSERT INTO groups (name, host, admins, participants, files, invites) VALUES (?, ?, ?, ?, ?, ?);', (group_name, host_name, pickle.dumps([host_name]), pickle.dumps([host_name]) , files, pickle.dumps({})))
        self.con.commit()

        return ALLOWED

    def join_group(self, group_name, username):
        # Check if group exists
        cur = self.con.cursor()
        cur.execute('SELECT host, admins, participants FROM groups WHERE name = ?;', (group_name,))
        rows = cur.fetchall()

        if len(rows) > 0:
            row = rows[0]

            # Check if host
            if username == row[0]:
                return ALLOWED_HOST

            # Check if admin
            elif username in pickle.loads(row[1]):
                return ALLOWED_ADMIN

            #Check in participants
            elif username in pickle.loads(row[2]):
                    return ALLOWED

            return NOT_IN_GROUP
        
        return GROUP_DOESNT_EXISTS

    def invite_to_group(self, group_name, inviting_user, invited_user):
        cur = self.con.cursor()
        cur.execute('SELECT admins, participants FROM groups WHERE name = ?;', (group_name,))
        rows = cur.fetchall()
        if len(rows) > 0:
            row = rows[0]
            # Check if admin
            if inviting_user in pickle.loads(row[0]):
                if invited_user not in pickle.loads(row[1]):

                    cur.execute('SELECT invites FROM groups WHERE name = ?;', (group_name,))
                    invites = pickle.loads(cur.fetchall()[0][0])
                    if invited_user not in invites:
                        invites[invited_user] = inviting_user
                        cur.execute('UPDATE groups SET invites = ?  WHERE name = ?;', (pickle.dumps(invites), group_name))
                        self.con.commit()
                    
                    return ALLOWED

                return ALREADY_LOGGED_IN

            return INSUFFICIENT_PERMS
        
        return GROUP_DOESNT_EXISTS

    def get_all_invites(self, username):
        invited_groups = 'A'
        cur = self.con.cursor()
        cur.execute('SELECT name, invites FROM groups')
        rows = cur.fetchall()
        for row in rows:
            invites = pickle.loads(row[1])
            if username in invites:
                invited_groups += row[0] + ':' + invites[username] + ','
        
        if invited_groups != 'A':
            invited_groups = invited_groups[:-1]

        return invited_groups 

    def accept_invite(self, group_name, username):
        cur = self.con.cursor()
        cur.execute('SELECT participants, invites FROM groups WHERE name = ?;', (group_name, ))
        rows = cur.fetchall()
        if len(rows) > 0:
            row = rows[0]
            invites = pickle.loads(row[1])
            if username in invites:
                del invites[username]

                participants = pickle.loads(row[0])
                participants.append(username)

                cur.execute('UPDATE groups SET participants = ?, invites = ? WHERE name = ?;', (pickle.dumps(participants), pickle.dumps(invites), group_name))
                self.con.commit()

    def reject_invite(self, group_name, username):
        cur = self.con.cursor()
        cur.execute('SELECT invites FROM groups WHERE name = ?;', (group_name, ))
        rows = cur.fetchall()
        if len(rows) > 0:
            row = rows[0]
            invites = pickle.loads(row[0])
            if username in invites:
                del invites[username]

                cur.execute('UPDATE groups SET invites = ? WHERE name = ?;', (pickle.dumps(invites), group_name))
                self.con.commit()

    def get_all_joined_groups(self, username):
        hosting_groups = []
        admin_groups = []
        joined_groups = []
        
        cur = self.con.cursor()
        cur.execute('SELECT name, host, admins, participants FROM groups')
        rows = cur.fetchall()

        for row in rows:
            # Host
            if row[1] == username:
                hosting_groups.append(row[0])
            
            # Admin
            elif username in pickle.loads(row[2]):
                admin_groups.append(row[0])

            # Participant
            elif username in pickle.loads(row[3]):
                joined_groups.append(row[0])

        return ",".join(hosting_groups) + ';' + ",".join(admin_groups) + ';' + ",".join(joined_groups)

    def get_group_files(self, group_name):
        # Get files
        cur = self.con.cursor()
        cur.execute('SELECT files FROM groups WHERE name = ?;', (group_name, ))
        rows = cur.fetchall()
        if len(rows) > 0:
            if len(rows[0]) > 0:
                return rows[0][0]
        return ''


    def set_group_files(self, files, group_name):
        cur = self.con.cursor()
        cur.execute('UPDATE groups SET files = ? WHERE name = ?;', (files, group_name))
        self.con.commit()