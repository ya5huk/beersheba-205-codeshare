const vscode = require("vscode");
const os = require("os");
const fs = require("fs");
const path = require("path");
const net = require("net");
const _ = require("lodash");

let globalCommunicationSock = null;
let latestPort = 0;

let info = vscode.window.createOutputChannel("Extension Info");
let globalSentChanges = [];

// Helper functions ->

/**
 * Function gets last modificated file (which shows the port a computer should connect to) from Temp folder in user's computer
 * @returns
 */
const getLatestFile = () => {
  const dir = path.join(os.tmpdir(), "LiveCodeShare");
  const files = fs.readdirSync(dir);
  let files_time = [];

  for (const file of files) {
    let curr = {};
    curr[file] = fs.statSync(path.join(dir, file)).mtime.getTime();
    files_time.push(curr);
  }
  files_time.sort((a, b) => Object.values(b)[0] - Object.values(a)[0]); // Sorts by mtime, from biggest (newest) to lowest
  return Object.keys(files_time[0])[0];
};

/**
 * Compares two lists with objects inside
 * @param {list<any>} list1
 * @param {list<any>} list2
 * @returns
 */
const compareObjectLists = (list1, list2) => {
  if (list1.length !== list2.length) {
    return false;
  }
  let index = 0;
  for (const ob of list1) {
    if (!_.isEqual(ob, list2[index])) {
      return false;
    }
    index++;
  }
  return true;
};

/**
 * Function validates if incoming changes is not ours change (that got to us because of looping)
 * @param {string} filepath
 * @param {any} changes
 * @returns
 */
const validateIncomingChange = (filepath, changes) => {
  let index = 0;
  info.appendLine(`Comparing ${JSON.stringify(changes)} in ${filepath} to:`);
  for (const globalChange of globalSentChanges) {
    if (
      globalChange.filepath === filepath &&
      _.isEqual(changes, globalChange.changes)
    ) {
      globalSentChanges.splice(index, 1); // Delete the change sentChanges
      return false;
    }
    index++;
  }
  return true;
};

/**
 * Validates change that leaves the program
 * @param {string} filepath
 * @param {any} changes
 * @returns
 */
const validateOutputChange = (filepath, changes) => {
  if (filepath.includes("extension-output")) return false;
  return true;
};

// Info transfer socket ->

/**
 * Function initializes socket for sending changes and receiving changes from POC
 * @param {int} connectPort
 * @returns
 */
const initCommunicationSock = (connectPort) => {
  let sendSocket = new net.Socket();
  sendSocket.connect({
    port: connectPort,
  });
  sendSocket.setEncoding("utf-8");

  sendSocket.on("connect", () => {
    const [address, port] = [sendSocket.remoteAddress, sendSocket.remotePort];
    vscode.window.showInformationMessage(
      `Editor connected to ${address}:${port}`
    );
    globalCommunicationSock = sendSocket;
  });

  // Receive data from POC
  // Change example -> {"filename":"New Text Document","changes":[{"changeStr":"d","start":{"line":6,"character":3},"end":{"line":6,"character":3}}]}
  sendSocket.on("data", (buff) => {
    try {
      info.appendLine(`Recv -> ${buff}`);
      const recv = JSON.parse(buff.toString());
      const { filename, changes } = recv;

      if (validateIncomingChange(filename, changes)) {
        for (const change of changes) {
          // Received
          const { changeStr, start, end } = change;
          vscode.window.visibleTextEditors.forEach((textDoc) => {
            const path = textDoc.document.fileName.split("\\");
            const currFilename = path[path.length - 1];
            if (currFilename === filename) {
              // We found file to change
              textDoc.edit((editBuilder) => {
                editBuilder.replace(
                  new vscode.Range(
                    new vscode.Position(start.line, start.character),
                    new vscode.Position(end.line, end.character)
                  ),
                  changeStr
                );
              });
            }
          });
        }
      } else {
        info.appendLine("Caught looping change.");
      }
    } catch (err) {
      info.appendLine(`Error receiving changes -> ${err}`);
    }
  });

  sendSocket.on("error", (err) => {
    info.appendLine(`Error in sending socket -> ${err}`);
  });

  sendSocket.on("end", (hadError) => {
    if (hadError) {
      info.appendLine("Send socket closed because of an error.");
    } else {
      info.appendLine("Send socket closed without any error.");
    }
  });

  return sendSocket;
};

// Plugin Functions

/**
 * Function runs on first activation of the extension
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {
  // Connect to last Temp port
  latestPort = parseInt(getLatestFile());
  initCommunicationSock(latestPort);

  info.appendLine(`Connecting to latest Temp port -> ${latestPort}`);

  vscode.workspace.onDidChangeTextDocument((event) => {
    // Capture change
    const path = event.document.fileName.split("\\");
    const filename = path[path.length - 1];
    let changes = [];
    event.contentChanges.forEach((change) => {
      changes.push({
        changeStr: change.text,
        start: change.range.start,
        end: change.range.end,
      });
    });

    // Send change
    if (
      globalCommunicationSock &&
      validateOutputChange(event.document.fileName, changes)
    ) {
      const [addr, port] = [
        globalCommunicationSock.remoteAddress,
        globalCommunicationSock.remotePort,
      ];
      info.appendLine(`Sent changes to ${addr}:${port}`);

      // When sending, store sent change to prevent looping
      // We will check the same change when received
      globalSentChanges.push({
        filepath: filename,
        changes: JSON.parse(JSON.stringify(changes)), // Some extremly dumb thing to make ._isEqual work...
      });

      globalCommunicationSock.write(
        JSON.stringify({
          filename: filename,
          changes: changes,
        })
      );
    } else {
      if (!globalCommunicationSock) {
        info.appendLine("Output socket is not initialized yet.");
      }
    }
  });

  context.subscriptions.push(
    vscode.commands.registerCommand("live-code-share.printLatestPort", () => {
      // Display a message box to the user
      try {
        vscode.window.showInformationMessage(latestPort);
      } catch (err) {
        info.appendLine(`Error -> ${err}`);
      }
    })
    // Manual Connection, should not be used in demo
    //
    // vscode.commands.registerCommand("live-code-share.connectToPort", () => {
    //   // Display a message box to the user
    //   try {
    //     vscode.window.showInputBox({ title: "Enter port " }).then((input) => {
    //       const port = parseInt(input);
    //       if (!sendSocket) {
    //         connectOutputSocket(port);
    //       } else {
    //         info.appendLine(
    //           `Sending to already connected -> ${sendSocket.address()}`
    //         );
    //       }
    //     });
    //   } catch (err) {
    //     info.appendLine("Error: " + err);
    //   }
    // })
  );
}

// this method is called when your extension is deactivated
function deactivate() {}

module.exports = {
  activate,
  deactivate,
};
