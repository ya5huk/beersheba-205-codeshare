﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Graphics
{
    /// <summary>
    /// Interaction logic for SignupWindow.xaml
    /// </summary>
    public partial class SignupControl : UserControl
    {

        private MainWindow _mainWindow;
        public SignupControl(MainWindow mainWindow)
        {
            InitializeComponent();
            _mainWindow = mainWindow;
        }

        private void ButtonSignup_Click(object sender, RoutedEventArgs e)
        {
            this.Error.Content = "";
            bool error = false;

            string username = txtUsername.Text;
            string password = txtPassword.Password;
            string confirmPassword = txtPasswordConfirm.Password;

            if (username == "")
            {
                txtUsername.BorderBrush = System.Windows.Media.Brushes.Red;
                error = true;
            }
            else
            {
                txtUsername.BorderBrush = System.Windows.Media.Brushes.White;
            }
            if (password == "")
            {
                txtPassword.BorderBrush = System.Windows.Media.Brushes.Red;
                error = true;
            }
            else
            {
                txtPassword.BorderBrush = System.Windows.Media.Brushes.White;
            }
            if (confirmPassword == "")
            {
                txtPasswordConfirm.BorderBrush = System.Windows.Media.Brushes.Red;
                error = true;
            }
            else
            {
                txtPasswordConfirm.BorderBrush = System.Windows.Media.Brushes.White;
            }
            if (confirmPassword != password)
            {
                this.Error.Content = "Please make sure the passwords match.";
                error = true;
            }
            // Make sure only valid characters were sent
            else if (!username.All(c => Char.IsLetterOrDigit(c)) || !password.All(c => Char.IsLetterOrDigit(c)))
            {
                this.Error.Content = "Username and Password may only contain letters and digits";
                error = true;
            }
            else
            {
                this.Error.Content = "";
            }

            if (!error)
            {
                //send signup, get response 
                _mainWindow._clientSock.Send("SUP" + username + "," + password);
                string loginResponse = _mainWindow._clientSock.Recv();

                if (loginResponse.Equals("100")) // Signup successful
                {
                    _mainWindow._clientSock.SetName(username);
                    _mainWindow.contentControl.Content = new MenuControl(_mainWindow);
                }
                else if (loginResponse.Equals("104"))
                {
                    this.Error.Content = "Name already used";
                }
            }
        }

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            _mainWindow.contentControl.Content = new LoginControl(_mainWindow);
        }
    }
}
