﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics
{
    static class Constants
    {
        public const int dataChunkSize = 1024;
        public const int bitsInByte = 8;
        public const int lengthSize = 32;

        public const int codeStartPos = 0;
        public const int lengthStartPos = 8;
        public const int dataStartPos = 40;
        public const int maxHighscoreListSize = 6;

        public const int loginCode = 101;
        public const int signupCode = 102;
        public const int logoutCode = 103;

        public const int joinRoomCode = 104;
        public const int createRoomCode = 105;
        public const int getRoomsCode = 106;
        public const int getPlayersCode = 107;

        public const int statisticsCode = 108;
        public const int totalStatisticsCode = 109;

        public const int startGameCode = 110;
        public const int closeRoomCode = 111;
        public const int leaveRoomCode = 112;
        public const int roomStateCode = 113;

        public const int port = 8083;
        public const string ip = "127.0.0.1";

        public const int reloadTime = 3000;
    }
}
