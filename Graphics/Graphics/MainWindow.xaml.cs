﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Graphics
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ClientSock _clientSock;
        public Thread _thread;
        public bool _isJoined = false;
        public bool _isInIntellijSession = false;
        public MainWindow()
        {
            Console.WriteLine("Enter port:");
            int port = Int32.Parse(Console.ReadLine());
            InitializeComponent();
            _clientSock = new ClientSock("", "127.0.0.1", port);

            this.contentControl.Content = new LoginControl(this);
        }
        protected override void OnClosed(EventArgs e) // On closing, sending signout message
        {
            if(_isJoined)
                _clientSock.Send("LEA");
            Console.Write("Bye!");
            base.OnClosed(e);
            Application.Current.Shutdown();
            return;
        }
    }
}
