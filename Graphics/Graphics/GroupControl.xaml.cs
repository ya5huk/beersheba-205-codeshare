using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Forms;

namespace Graphics
{
    /// <summary>
    /// Interaction logic for GroupControl.xaml
    /// </summary>
    /// 

    public partial class GroupControl : System.Windows.Controls.UserControl
    {

        private MainWindow _mainWindow;
        private string _name;
        private bool _isHost;
        private bool _isAdmin;
        Thread _playersRefresher; // so we can abort it in exit

        public GroupControl(MainWindow mainWindow, string name, bool isAdmin, bool isHost)
        {
            InitializeComponent();
            _mainWindow = mainWindow;
            _name = name;
            _isHost = isHost;
            _isAdmin = isAdmin;
            _playersRefresher = new Thread(ReloadScreen);
            _mainWindow._thread = _playersRefresher;

            if (!_isAdmin)
            {
                labelInvite.Visibility = Visibility.Hidden;
                txtInvite.Visibility = Visibility.Hidden;
                InviteButton.Visibility = Visibility.Hidden;
            }
            else
            {
                //ButtonLeaveGame.Visibility = Visibility.Hidden; // because if an admin leaves no one can close
            }


            _playersRefresher.Start();


            /*
            Label headerLabel = new Label();
            headerLabel.Content = "Players";
            headerLabel.Foreground = Brushes.White;
            headerLabel.FontFamily = new FontFamily("Lily Script One");
            headerLabel.FontSize = 17;
            PlayersLabelsPanel.Children.Add(headerLabel);
            */

        }

        private void addPlayerText(string player)
        {
            System.Windows.Application.Current.Dispatcher.Invoke((Action)delegate
            { // This line is to activate this code, otherwise does not work!
                System.Windows.Controls.Label playerLabel = new System.Windows.Controls.Label()
                {
                    Content = player,
                    Foreground = Brushes.White,
                    FontFamily = new FontFamily("Lily Script One"),
                    FontSize = 15
                };

                //TODO Check for admin
                //if (player == _players[0])
                //    playerLabel.Content += " (Admin)";

                PlayersLabelsPanel.Children.Add(playerLabel);


            });
        }

        private void removePlayerText(string player)
        {
            System.Windows.Application.Current.Dispatcher.Invoke((Action)delegate
            { // This line is to activate this code, otherwise does not work!

                foreach (System.Windows.Controls.Label label in PlayersLabelsPanel.Children) // delete by content
                {
                    if (label.Content.Equals(player))
                    {
                        PlayersLabelsPanel.Children.Remove(label);
                        break;
                    }
                }

            });
        }
        private void ReloadScreen()
        {
            List<string> currGroupNames = new List<string>();
            while (true)
            {
                /*
                // Room refreshing
                if (interruptRefresh == false && roomsDataResponse != null && roomsDataResponse.players != null && roomsDataResponse.players.ToList().Count > 0)
                {
                    foreach (string name in roomsDataResponse.players)
                    {
                        if (!currRoomNames.Contains(name))
                        {
                            addPlayerText(name);
                            currRoomNames.Add(name);
                            // A way to not add the same player every couple of seconds
                        }
                    }
                    // Delete not relevant names:
                    if (currRoomNames.Count > 0)
                    {
                        foreach (string name in currRoomNames.ToArray())
                        {
                            if (!roomsDataResponse.players.Contains(name)) // not relevant
                            {
                                removePlayerText(name);
                                currRoomNames.Remove(name);
                            }
                        }
                    }
                }
                else
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    { // This line is to activate this code, otherwise does not work!
                        if (!_isAdmin)
                        {
                            ButtonLeaveGame_Click(new object(), new RoutedEventArgs());
                        }
                        else
                        {
                            _mainWindow.contentControl.Content = new MenuControl(_mainWindow);
                        }
                    });
                    
                }
                */


                //Thread.Sleep(Constants.reloadTime);
            }

        }
        private void ButtonEnterSeasion_Click(object sender, RoutedEventArgs e)
        {
            _mainWindow._clientSock.Send("ENT" + txtRepository.Text);
            EnterSeasion.Content = "Leave Seasion";
            EnterSeasion.Click -= ButtonEnterSeasion_Click;
            EnterSeasion.Click += ButtonLeaveSeasion_Click;
            ButtonLeaveGroup.Visibility = Visibility.Hidden;
            _mainWindow._isJoined = true;
            _playersRefresher.Abort();

            // Turn off group settings in session
            SelectRepoCanvas.Visibility = Visibility.Hidden;
            RepoPanel.Visibility = Visibility.Hidden;
            InvitePanel.Visibility = Visibility.Hidden;

            CodeTogether.Visibility = Visibility.Visible;

        }

        private void CodeTogether_Click(object sender, RoutedEventArgs e)
        {
            _mainWindow._isInIntellijSession = true;
            _mainWindow._clientSock.Send("EDI"); // Will turn off casual update and force everyone to open

            // Option to start regular sync
            CodeTogether.Visibility = Visibility.Hidden;
            SyncFiles.Visibility = Visibility.Visible;
        }

        private void SyncFiles_Click(object sender, RoutedEventArgs e)
        {
            _mainWindow._clientSock.Send("SYN");

            // Option to start open Intellij session
            CodeTogether.Visibility = Visibility.Visible;
            SyncFiles.Visibility = Visibility.Hidden;
        }

        private void ButtonLeaveSeasion_Click(object sender, RoutedEventArgs e)
        {
            _mainWindow._clientSock.Send("LEA");
            EnterSeasion.Content = "Enter Seasion";
            EnterSeasion.Click -= ButtonLeaveSeasion_Click;
            EnterSeasion.Click += ButtonEnterSeasion_Click;
            ButtonLeaveGroup.Visibility = Visibility.Visible;
            _mainWindow._isJoined = false;
            _playersRefresher.Abort();



            // Turn on group settings in session again
            SelectRepoCanvas.Visibility = Visibility.Visible;
            RepoPanel.Visibility = Visibility.Visible;
            InvitePanel.Visibility = Visibility.Visible;

            CodeTogether.Visibility = Visibility.Hidden;
            SyncFiles.Visibility = Visibility.Hidden;
        }
        private void ButtonLeaveGroup_Click(object sender, RoutedEventArgs e)
        {
            _playersRefresher.Abort();
            _mainWindow._clientSock.Send("BAC");
            _mainWindow.contentControl.Content = new MenuControl(_mainWindow);


        }
        private void ButtonSelectRepo_Click(object sender, RoutedEventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    txtRepository.Text = fbd.SelectedPath;
                    if (txtRepository.Text[txtRepository.Text.Length - 1] != '/' && txtRepository.Text[txtRepository.Text.Length - 1] != '\\')
                    {
                        txtRepository.Text += '\\';
                    }
                    txtRepository.Text += _name;
                }
            }
        }

        private void ButtonInvite_Click(object sender, RoutedEventArgs e)
        {
            if(!_isAdmin)
            {
                return;
            }
            string invitedName = txtInvite.Text;
            this.Error.Foreground = System.Windows.Media.Brushes.Red;

            if (invitedName == "" || (!invitedName.All(c => Char.IsLetterOrDigit(c))))
            {
                txtInvite.BorderBrush = System.Windows.Media.Brushes.Red;
                this.Error.Content = "Invalid name";
            }
            else
            {
                _mainWindow._clientSock.Send("INV" + invitedName);
                string inviteResponse = _mainWindow._clientSock.Recv();

                if (inviteResponse.Equals("100"))
                {
                    this.Error.Content = invitedName + " Invited!";
                    txtInvite.BorderBrush = System.Windows.Media.Brushes.Lime;
                    this.Error.Foreground = System.Windows.Media.Brushes.Lime;
                }
                else if (inviteResponse.Equals("102"))
                {
                    this.Error.Content =  invitedName + " already in group";
                    txtInvite.BorderBrush = System.Windows.Media.Brushes.Red;
                }
                else if (inviteResponse.Equals("114"))
                {
                    this.Error.Content = "insufficient permissions";
                    txtInvite.BorderBrush = System.Windows.Media.Brushes.Red;
                }
                else if (inviteResponse.Equals("113"))
                {
                    this.Error.Content = "Group doesn't exists";
                    txtInvite.BorderBrush = System.Windows.Media.Brushes.Red;
                }
            }
            
        }

        private void WaitForAdmin()
        {
            /*
            // we only recieve here and check for code:
            byte[] recievedBytes = new byte[32768];
            int bytes = _mainWindow._clientSock.GetSocket().Receive(recievedBytes, recievedBytes.Length, 0);
            string recievedMsg = Encoding.ASCII.GetString(recievedBytes, 0, bytes);
            DeserializedMessage msgInfo = new DeserializedMessage(recievedMsg);
            if (msgInfo.GetCode() == Constants.startGameCode && msgInfo.GetData().Contains("1"))
            {
                MessageBox.Show("Game has started", "Status");
            }
            else
            {
                // Something went wrong
            }
            */
        }
       
    } 
}
