﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Graphics
{
    public static class HelperFunctions
    {
        public static string DictionaryToString(Dictionary<string, string> dictionary)
        {
            string dictionaryString = "{";
            foreach (KeyValuePair<string, string> keyValues in dictionary)
            {
                dictionaryString += '"' + keyValues.Key + '"' + " : " + '"' + keyValues.Value + '"' +  ", ";
            }
            return dictionaryString.TrimEnd(',', ' ') + "}";
        }

        public static string ToBinary(int code, string data, int length)
        {
            // format all data together to binary string
            string binaryCode = Convert.ToString(code, 2);
            binaryCode = String.Concat(Enumerable.Repeat("0", Constants.bitsInByte - binaryCode.Length)) + binaryCode; // Adds 0 so binary num will be 8 bytes long

            string binaryLength = Convert.ToString(length, 2);
            binaryLength = String.Concat(Enumerable.Repeat("0", Constants.lengthSize - binaryLength.Length)) + binaryLength;

            string binaryData = "";
            foreach (char letter in data.ToCharArray())
            {
                string binaryChar = Convert.ToString(letter, 2);
                binaryChar = String.Concat(Enumerable.Repeat("0", Constants.bitsInByte - binaryChar.Length)) + binaryChar;
                binaryData += binaryChar;
            }
            // Now we just sum up the binary vars
            return binaryCode + binaryLength + binaryData;
        }
    }
    // Things all Request classes must have
    public interface IRequest
    {
        string GetDataInJson();
        string Serialize();
    }
    class AuthRequest : IRequest // Authentication request -> Login or signup
    {
        bool _isSignup;
        string _username;
        string _password;
        string _email;

        public AuthRequest(bool isSignup, string username, string password) // for login
        {
            _isSignup = isSignup;
            _username = username;
            _password = password;
            _email = "";
        }

        public AuthRequest(bool isSignup, string username, string password, string email) // for signup
        {
            _isSignup = isSignup;
            _username = username;
            _password = password;
            _email = email;
        }
        public string GetDataInJson() // Function returns 
        {
            Dictionary<string, string> dataDict = new Dictionary<string, string>();
            dataDict["username"] = _username;
            dataDict["password"] = _password;
            if (_isSignup)
            {
                dataDict["email"] = _email;
            }

            return HelperFunctions.DictionaryToString(dataDict);

        }

        public string Serialize()
        {
            int code = _isSignup ? Constants.signupCode : Constants.loginCode;
            string data = GetDataInJson();
            int length = data.Length * Constants.bitsInByte;

            return HelperFunctions.ToBinary(code, data, length);
        }

    }

    class OwnStatisticsRequest : IRequest
    {
        public OwnStatisticsRequest() {}
        public string GetDataInJson() // Function returns empty json because it is not needed
        {
            return "{}";
        }
        public string Serialize()
        {
            int code = Constants.statisticsCode;
            string data = GetDataInJson();
            int length = data.Length * Constants.bitsInByte;
            
            return HelperFunctions.ToBinary(code, data, length);
            
        }
    }

    // NOTE! ALL REQUESTS WITH CODE ONLY CAN BE COMBINED TO ONE REQUEST WITH FIELD CODE!!!!!!!

    class TotalStatisticsRequest : IRequest
    {
        public TotalStatisticsRequest() { }
        public string GetDataInJson() // Function returns empty json because it is not needed
        {
            return "{}";
        }
        public string Serialize()
        {
            int code = Constants.totalStatisticsCode;
            string data = GetDataInJson();
            int length = data.Length * Constants.bitsInByte;

            return HelperFunctions.ToBinary(code, data, length);
        }
    }

    class LogoutRequest : IRequest
    {
        string _username;
        public LogoutRequest(string username) 
        {
            _username = username;
        }
        public string GetDataInJson() // Function returns 
        {
            Dictionary<string, string> dataDict = new Dictionary<string, string>();
            dataDict["username"] = _username;

            return HelperFunctions.DictionaryToString(dataDict);
        }
        public string Serialize()
        {
            int code = Constants.logoutCode;
            string data = GetDataInJson();
            int length = data.Length * Constants.bitsInByte;

            return HelperFunctions.ToBinary(code, data, length);
        }
    }

    class RoomsRequest : IRequest // Authentication request -> Login or signup
    {
        public RoomsRequest() {}
        public string GetDataInJson() // Function returns 
        {
            return "{}";
        }

        public string Serialize()
        {
            int code = Constants.getRoomsCode;
            string data = GetDataInJson();
            int length = data.Length * Constants.bitsInByte;

            return HelperFunctions.ToBinary(code, data, length);
        }

    }

    class RoomPlayersRequest : IRequest // Authentication request -> Login or signup
    {
        int _roomId;
        public RoomPlayersRequest(int id) 
        {
            _roomId = id;
        }
        public string GetDataInJson() // Function returns 
        {
            Dictionary<string, string> dataDict = new Dictionary<string, string>();
            dataDict["room_id"] = _roomId.ToString();

            return HelperFunctions.DictionaryToString(dataDict);
        }

        public string Serialize()
        {
            int code = Constants.getPlayersCode;
            string data = GetDataInJson();
            int length = data.Length * Constants.bitsInByte;

            return HelperFunctions.ToBinary(code, data, length);
        }

    }

    class CreateRoomRequest : IRequest // Authentication request -> Login or signup
    {
        string _roomName;
        int _maxUsers;
        int _questionCount;
        int _answerTimeout;
        public CreateRoomRequest(string name, int users, int questions, int timeout)
        {
            _roomName = name;
            _maxUsers = users;
            _questionCount = questions;
            _answerTimeout = timeout;
        }
        public string GetDataInJson() // Function returns 
        {
            Dictionary<string, string> dataDict = new Dictionary<string, string>();
            dataDict["roomName"] = _roomName;
            dataDict["maxUsers"] = _maxUsers.ToString();
            dataDict["questionCount"] = _questionCount.ToString();
            dataDict["answerTimeout"] = _answerTimeout.ToString();

            return HelperFunctions.DictionaryToString(dataDict);
        }

        public string Serialize()
        {
            int code = Constants.createRoomCode;
            string data = GetDataInJson();
            int length = data.Length * Constants.bitsInByte;

            return HelperFunctions.ToBinary(code, data, length);
        }

    }

    class JoinRoomRequest : IRequest 
    {
        int _roomId;
        public JoinRoomRequest(int id)
        {
            _roomId = id;
        }
        public string GetDataInJson() // Function returns 
        {
            Dictionary<string, string> dataDict = new Dictionary<string, string>();
            dataDict["room_id"] = _roomId.ToString();

            return HelperFunctions.DictionaryToString(dataDict);
        }

        public string Serialize()
        {
            int code = Constants.joinRoomCode;
            string data = GetDataInJson();
            int length = data.Length * Constants.bitsInByte;

            return HelperFunctions.ToBinary(code, data, length);
        }

    }

    class RoomStateRequest : IRequest 
    {
        public RoomStateRequest() {}
        public string GetDataInJson() // Function returns 
        {
            Dictionary<string, string> dataDict = new Dictionary<string, string>();

            return HelperFunctions.DictionaryToString(dataDict);
        }

        public string Serialize()
        {
            int code = Constants.roomStateCode;
            string data = GetDataInJson();
            int length = data.Length * Constants.bitsInByte;

            return HelperFunctions.ToBinary(code, data, length);
        }

    }

    class LeaveRoomRequest : IRequest
    {
        public LeaveRoomRequest() { }
        public string GetDataInJson() // Function returns 
        {
            Dictionary<string, string> dataDict = new Dictionary<string, string>();

            return HelperFunctions.DictionaryToString(dataDict);
        }

        public string Serialize()
        {
            int code = Constants.leaveRoomCode;
            string data = GetDataInJson();
            int length = data.Length * Constants.bitsInByte;

            return HelperFunctions.ToBinary(code, data, length);
        }

    }

    class closeRoomRequest : IRequest
    {
        public closeRoomRequest() { }
        public string GetDataInJson() // Function returns 
        {
            Dictionary<string, string> dataDict = new Dictionary<string, string>();

            return HelperFunctions.DictionaryToString(dataDict);
        }

        public string Serialize()
        {
            int code = Constants.closeRoomCode;
            string data = GetDataInJson();
            int length = data.Length * Constants.bitsInByte;

            return HelperFunctions.ToBinary(code, data, length);
        }
    }

    class StartGameRequest : IRequest
    {
        public StartGameRequest() { }
        public string GetDataInJson() // Function returns 
        {
            Dictionary<string, string> dataDict = new Dictionary<string, string>();

            return HelperFunctions.DictionaryToString(dataDict);
        }

        public string Serialize()
        {
            int code = Constants.startGameCode;
            string data = GetDataInJson();
            int length = data.Length * Constants.bitsInByte;

            return HelperFunctions.ToBinary(code, data, length);
        }
    }

}
