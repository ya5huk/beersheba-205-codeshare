﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace Graphics
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginControl : UserControl
    {

        private MainWindow _mainWindow;

        public LoginControl(MainWindow mainWindow)
        {
            InitializeComponent();
            _mainWindow = mainWindow;
        }

        private void ButtonLogin_Click(object sender, RoutedEventArgs e)
        {
            this.Error.Content = "";
            bool error = false;

            string username = txtUsername.Text;
            string password = txtPassword.Password;

            if (username == "")
            {
                txtUsername.BorderBrush = System.Windows.Media.Brushes.Red;
                error = true;
            }
            else
            {
                txtUsername.BorderBrush = System.Windows.Media.Brushes.White;
            }
            if (password == "")
            {
                txtPassword.BorderBrush = System.Windows.Media.Brushes.Red;
                error = true;
            }
            else
            {
                txtPassword.BorderBrush = System.Windows.Media.Brushes.White;
            }

            // Make sure only valid characters were sent
            if (!username.All(c => Char.IsLetterOrDigit(c)) || !password.All(c => Char.IsLetterOrDigit(c)))
            {
                this.Error.Content = "Username and Password may only contain letters and digits";
                error = true;
            }

            if (!error)
            {
                //send login, get response 
                _mainWindow._clientSock.Send("LOG" + username + "," + password);
                string loginResponse = _mainWindow._clientSock.Recv();

                if (loginResponse.Equals("100")) // Login successful
                {
                    _mainWindow._clientSock.SetName(username);
                    _mainWindow.contentControl.Content = new MenuControl(_mainWindow);
                }
                else if(loginResponse.Equals("101"))
                {
                    this.Error.Content = "Incorrect password";
                }
                else if (loginResponse.Equals("102"))
                {
                    this.Error.Content = "User already logged in";
                }
                else if (loginResponse.Equals("103"))
                {
                    this.Error.Content = "User wasn't found";
                }
            }
        }

        private void ButtonSignup_Click(object sender, RoutedEventArgs e)
        {
            _mainWindow.contentControl.Content = new SignupControl(_mainWindow);
        }
    }
}
