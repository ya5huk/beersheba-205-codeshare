﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Graphics
{
    /// <summary>
    /// Interaction logic for JoinGroupControl.xaml
    /// </summary>
    public partial class JoinGroupControl : UserControl
    {

        private MainWindow _mainWindow;
        public JoinGroupControl(MainWindow mainWindow)
        {
            InitializeComponent();
            _mainWindow = mainWindow;

            ReloadScreen();
        }

        private void ReloadScreen()
        {
            _mainWindow._clientSock.Send("ALL");
            string ret = _mainWindow._clientSock.Recv();
            string[] groups = ret.Split(';');
            string[] hostingGroups = groups[0].Split(',');
            string[] admingGroups = groups[1].Split(',');
            string[] joinedGroups = groups[2].Split(',');

            if (hostingGroups[0] != "")
            {
                foreach (string group in hostingGroups)
                {
                    AddGroupButton(group);
                }
            }
            if (admingGroups[0] != "")
            {
                foreach (string group in admingGroups)
                {
                    AddGroupButton(group);
                }
            }
            if (joinedGroups[0] != "")
            {
                foreach (string group in joinedGroups)
                {
                    AddGroupButton(group);
                }
            }
        }

        private void AddGroupButton(string group)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate { // This line is to activate this code, otherwise does not work!
                Button groupButton = new Button()
                {
                    Name = group, 
                    Content = group,
                    BorderThickness = new Thickness(0, 0, 0, 0),
                    Margin = new Thickness(110, 5, 110, 5),
                    Background = null,
                    Foreground = Brushes.White,
                    FontFamily = new FontFamily("Lily Script One"),
                    FontSize = 18,
                    Height = 25
                };
                groupButton.Click += ButtonJoinGroup_Click;

                Trigger tg = new Trigger()
                {
                    Property = Button.IsMouseOverProperty,
                    Value = true
                };

                tg.Setters.Add(new Setter()
                {
                    Property = Control.BackgroundProperty,
                    Value = Brushes.DarkGoldenrod
                });

                Style st = new Style();
                st.Triggers.Add(tg);

                groupButton.Style = st;

                GroupsButtonPanel.Children.Add(groupButton);
            });
            
        }
        private void ButtonJoinGroup_Click(object sender, RoutedEventArgs e)
        {
            string groupName = (sender as Button).Content.ToString();
            _mainWindow._clientSock.Send(("JOI" + groupName));
            string joinGroupResponse = _mainWindow._clientSock.Recv();
            if (joinGroupResponse.Equals("110"))
            {
                _mainWindow.contentControl.Content = new GroupControl(_mainWindow, groupName, true, true);
            }
            else if (joinGroupResponse.Equals("111"))
            {
                _mainWindow.contentControl.Content = new GroupControl(_mainWindow, groupName, true, false);
            }
            else if (joinGroupResponse.Equals("100"))
            {
                _mainWindow.contentControl.Content = new GroupControl(_mainWindow, groupName, false, false);
            }
            else
            {
                // Something went wrong we got {"status": 0}
            }
        }

        private void Invites_Click(object sender, RoutedEventArgs e)
        {
            _mainWindow.contentControl.Content = new InvitesControl(_mainWindow);
        }

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            _mainWindow.contentControl.Content = new MenuControl(_mainWindow);
        }
    }
}
