﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Graphics
{
    /// <summary>
    /// Interaction logic for InvitesControl.xaml
    /// </summary>
    public partial class InvitesControl : UserControl
    {
        private MainWindow _mainWindow;
        public InvitesControl(MainWindow mainWindow)
        {
            InitializeComponent();
            _mainWindow = mainWindow;

            ReloadScreen();
        }

        private void ReloadScreen()
        {
            InvitesPanel.Children.Clear();
            AcceptPanel.Children.Clear();
            RejectPanel.Children.Clear();

            Label title = new Label()
            {
                Content = "Pending Invites",
                HorizontalContentAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                FontFamily = new FontFamily("Lily Script One"),
                FontSize = 25,
            };

            InvitesPanel.Children.Add(title);

            _mainWindow._clientSock.Send("AIN");
            string ret = _mainWindow._clientSock.Recv().Substring(1);
            string[] groups = ret.Split(',');

            string[] data;

            if (groups[0] != "")
            {
                foreach (string group in groups)
                {
                    data = group.Split(':');

                    AddGroupButton(data[0], data[1]);
                }
            }
        }

        private void AddGroupButton(string group, string inviting_user)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate { // This line is to activate this code, otherwise does not work!

                Label groupLabel = new Label()
                {
                    Name = group,
                    Content = group + ", by: " + inviting_user,
                    BorderThickness = new Thickness(0, 0, 0, 0),
                    Background = null,
                    Foreground = Brushes.White,
                    FontFamily = new FontFamily("Lily Script One"),
                    FontSize = 18,
                    Height = 40
                };
                Button acceptGroupButton = new Button()
                {
                    Name = "Accept" + group,
                    Content = "✓",
                    BorderThickness = new Thickness(0, 0, 0, 0),
                    Background = null,
                    Foreground = Brushes.White,
                    FontFamily = new FontFamily("Lily Script One"),
                    FontSize = 18,
                    Height = 40
                };
                Button rejectGroupButton = new Button()
                {
                    Name = "reject" + group,
                    Content = "✘",
                    BorderThickness = new Thickness(0, 0, 0, 0),
                    Background = null,
                    Foreground = Brushes.White,
                    FontFamily = new FontFamily("Lily Script One"),
                    FontSize = 18,
                    Height = 40
                };

                acceptGroupButton.Click += ButtonAccept_Click;
                rejectGroupButton.Click += ButtonReject_Click;

                Trigger tg = new Trigger()
                {
                    Property = Button.IsMouseOverProperty,
                    Value = true
                };

                tg.Setters.Add(new Setter()
                {
                    Property = Control.BackgroundProperty,
                    Value = Brushes.DarkGoldenrod
                });

                Style st = new Style();
                st.Triggers.Add(tg);

                acceptGroupButton.Style = st;
                rejectGroupButton.Style = st;

                InvitesPanel.Children.Add(groupLabel);
                AcceptPanel.Children.Add(acceptGroupButton);
                RejectPanel.Children.Add(rejectGroupButton);
            });

        }
        private void ButtonAccept_Click(object sender, RoutedEventArgs e)
        {
            string groupName = (sender as Button).Name.ToString().Substring(6);
            _mainWindow._clientSock.Send("ACC" + groupName);

            Thread.Sleep(500);
            ReloadScreen();
        }
        private void ButtonReject_Click(object sender, RoutedEventArgs e)
        {
            string groupName = (sender as Button).Name.ToString().Substring(6);
            _mainWindow._clientSock.Send("REJ" + groupName);

            Thread.Sleep(500);
            ReloadScreen();
        }
        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            _mainWindow.contentControl.Content = new JoinGroupControl(_mainWindow);
        }
    }
}
