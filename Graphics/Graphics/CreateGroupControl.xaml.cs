﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;

namespace Graphics
{
    /// <summary>
    /// Interaction logic for CreateGroupControl.xaml
    /// </summary>
    public partial class CreateGroupControl : System.Windows.Controls.UserControl
    {

        private MainWindow _mainWindow;

        public CreateGroupControl(MainWindow mainWindow)
        {
            InitializeComponent();
            _mainWindow = mainWindow;
        }

        private void ButtonSelectRepo_Click(object sender, RoutedEventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    txtRepository.Text = fbd.SelectedPath;
                }
            }
        }
        private void ButtonCreateGroup_Click(object sender, RoutedEventArgs e)
        {
            bool error = false;

            string groupName = txtGroupName.Text;
            string repository = txtRepository.Text;

            if (groupName == "")
            {
                txtGroupName.BorderBrush = System.Windows.Media.Brushes.Red;
                error = true;
            }
            else
            {
                txtGroupName.BorderBrush = System.Windows.Media.Brushes.White;
            }
            if (!Directory.Exists(repository))
            {
                txtGroupName.BorderBrush = System.Windows.Media.Brushes.Red;
                error = true;
            }
            else
            {
                txtGroupName.BorderBrush = System.Windows.Media.Brushes.White;
            }

            if (!error)
            {
                _mainWindow._clientSock.Send(("CRE" + groupName + "," + repository));
                string createGroupResponse = _mainWindow._clientSock.Recv();

                if (createGroupResponse.Equals("100")) // Signup successful
                {
                    _mainWindow.contentControl.Content = new GroupControl(_mainWindow, groupName, true, true);
                }
                else if (createGroupResponse.Equals("104"))
                {
                    this.Error.Content = "Name already used";
                }
            }
            else
            {
                //TODO throw passwords doesnt match
            }
        }
        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            _mainWindow.contentControl.Content = new MenuControl(_mainWindow);
        }
    }
}
