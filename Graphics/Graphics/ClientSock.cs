﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Graphics
{
    public class ClientSock
    {
        Socket _socket;
        string _name; // Just a way to relate sockets to user

        public ClientSock(string name, string server, int port)
        {
            Socket sock = ConnectSocket(server, port);
            while (sock == null)
            {
                sock = ConnectSocket(server, port); // don't proceed until there is connection
                // Now client can be opened before server
            }
            _socket = sock;
            _name = name;
            
            
        }

        public ClientSock(string name, Socket sock)
        {
            _name = name;
            _socket = sock;
        }

        public void SetName(string name)
        {
            _name = name;
        }

        public string GetName()
        {
            return _name;
        }

        public Socket GetSocket()
        {
            return _socket;
        }

        public void Send(string msg)
        {
            _socket.Send(Encoding.ASCII.GetBytes(msg)); // Send encoded msg
        }

        public string Recv()
        {
            byte[] recievedBytes = new byte[32768];
            int bytes = _socket.Receive(recievedBytes, recievedBytes.Length, 0);
            string recievedMsg = Encoding.ASCII.GetString(recievedBytes, 0, bytes);

            return recievedMsg;
        }
        private static Socket ConnectSocket(string server, int port)
        {
            IPAddress ipAddress = Dns.GetHostAddresses(server)[0];
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

            Socket sock = new Socket(ipAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);

            try
            {
                // Connect to Remote EndPoint  
                sock.Connect(remoteEP);
            }
            catch (SocketException se)
            {
                Console.WriteLine("SocketException : {0}", se.ToString());
                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected exception : {0}", e.ToString());
                return null;
            }

            return sock;
        }
    }
}
