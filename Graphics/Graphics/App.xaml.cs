﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Graphics
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
        }
        protected override void OnExit(ExitEventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
            Console.Write("Bye!!!");
            base.OnExit(e);
            Application.Current.Shutdown();
            return;
        }
    }
}
