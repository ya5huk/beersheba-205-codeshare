﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Graphics
{
    /// <summary>
    /// Interaction logic for MenuWindow.xaml
    /// </summary>
    public partial class MenuControl : UserControl
    {

        private MainWindow _mainWindow;
        
        public MenuControl(MainWindow mainWindow)
        {
            InitializeComponent();
            _mainWindow = mainWindow;
        }

        private void ButtonQuit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown(); // Turn off
        }

        private void ButtonJoinRoom_Click(object sender, RoutedEventArgs e)
        {
            _mainWindow.contentControl.Content = new JoinGroupControl(_mainWindow);
        }

        private void ButtonCreateRoom_Click(object sender, RoutedEventArgs e)
        {
            _mainWindow.contentControl.Content = new CreateGroupControl(_mainWindow);
        }
    }
}
